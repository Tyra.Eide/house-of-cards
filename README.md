# House of Cards

House of Cards is a platformer game where you play as Janus the Joker, climbing up the four towers of clubs,
diamonds, heart and spades to free the respective kings of a curse that has befallen them.
Janus has to climb ladders, jump from platform to platform, and avoid getting hit by the guards that wander around the rooms.
Janus can shoot cards at the guards, which will wipe them out. The higher the level, the more lives each guard has.
To beat the game, Janus has to clear every level and free all the Kings from the curse!



## HOW TO PLAY:

Launch the game and choose "Start Game" in the main menu.
A brief introduction text will appear, before the first level loads.

---> **Movement sideways**

    To move the character sideways, use the left and right arrows on the keybaord.
    The character can move sideways when not stunned by a monster or when climbing.
    When the character steps off a platform, it will fall until it lands on another one.
    If the player falls from a medium height, 25 damage will be dealt. If the player falls
    from a great height, 50 damage is dealt.

    Possible expansions of the game include the possibility of jumping off a ladder
    by moving sideways away from it.

---> **Jumping**

    The space bar is used to jump, and the character will stop to charge the jump before launching into
    the air. It is only possible to jump when standing on a platform. It is not possible to jump
    out of a ladder, although it is possible to jump onto a ladder by jumping and pressing either the up
    or down key when coming in contact with the ladder.

    Possible expansions of the game include double jumping and charged jumps.

---> **Climbing**

    The up and down arrows are used to climb ladders. Only when climbing can these be used.
    It is not possible to move any other direction when climbing a ladder, and it is also not
    possible to attack or jump when climbing.

---> **Attacking**

    To shoot cards at enemies, use the A button on the keyboard. The player can only attack when standing or
    running on a platform, and the player can not attack mid jump, fall or climb.

    Possible expansions of the game include the ability to attack while jumping and climbing.

---> **Health**

    The health of the player is shown in the top left corner in the form of heart icons. When losing health, the
    red filling of the heart will slowly decrease, and when the heart is empty, it disappears, signalling how many lives
    the player has left. When all the health has depleated, the character dies and the game is over. At the current point,
    the game will have to be played again from the start.

    Possible expansions of the game include the level being reset when losing a life, and the player being set back
    five levels at death instead of having to replay from the start. The possibility to upgrade the amount of lives
    if also a possible expansion.

---> **Getting to the next level**

    To progress through the levels, the player has to manouver their way from the starting point at the bottom of the screen
    to the very top of the level, where a door in the form of a black square awaits them. This door takes the player to the 
    next level. It is not neccesary to defeat any enemies to be able to go through the door, but in many cases, it will be 
    neccesary to clear the way to be able to reach the door.

---> **Naming of the levels**

    There are four towers to climb, one for each suit in a deck of cards (clubs, diamond, hearts, spades). Each level ranges from A (ace, 1) to K (king, 13).
    The levels are therefore named as the number followed by the suit of the current tower.



## EXPANSIONS THAT I DID NOT GET TIME TO IMPLEMENT:

    -> A cursed card the player has to wake to unlock the door to the next level. This would be the card that names the level.
    -> A mini-boss at level 5, 11 and 12 in each tower.
    -> A boss at the end of each tower, acting as the cursed King of the tower.
    -> A final boss at the end of the game.
    -> Special attacks and powers given to the player by each King. 
        These powers would be a shield, a healing function, a long range attack and a powerful short range  attack.
    -> The possibility to open chests around the levels. The chests would contain either hearts to fill or expand the player's health or coins.
    -> An upgrades shop where the player can purchase upgrades in weapons and special powers with coins collected from the levels.
    -> A better show of the storyline with a longer introduction that also serves as a tutorial.
