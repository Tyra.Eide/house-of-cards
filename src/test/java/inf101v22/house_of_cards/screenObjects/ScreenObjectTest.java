package inf101v22.house_of_cards.screenObjects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Dimension;
import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

/** Testing of the class ScreenObject. */
public class ScreenObjectTest {
    ScreenObject o = new ScreenObject(10, 30, new Dimension(20, 30));
    
    @Test
    void ScreenObjectTestPositionTests() {
        
        //set position
        o.setPosition(30, 90);

        assertEquals(30, o.getX());
        assertEquals(90, o.getY());

        //move position
        o.movePosition(10, 10);

        assertEquals(40, o.getX());
        assertEquals(100, o.getY());
    }

    @Test
    void ScreenObjectTestDimensionAndHitboxTest() {

        assertEquals(o.getDimension().width, o.getHitbox().width);
        assertEquals(o.getDimension().height, o.getHitbox().height);

        int deltaX = 10;
        int deltaY = 20;

        o.moveHitbox(deltaX, deltaY);
        assertEquals(o.getX() + deltaX, o.getHitbox().x);
        assertEquals(o.getY() + deltaY, o.getHitbox().y);

        o.setHitbox(new Rectangle(20, 30, 40, 10));

        assertEquals(20, o.getHitbox().x);
        assertEquals(30, o.getHitbox().y);
        assertEquals(40, o.getHitbox().width);
        assertEquals(10, o.getHitbox().height);
    }

    @Test
    void ScreenObjectTestIsColliding() {
        //check when collsiion is happening
        ScreenObject a = new ScreenObject(10, 10, new Dimension(100, 100));
        ScreenObject b = new ScreenObject(50, 50, new Dimension(100, 100));

        assertTrue(a.isColliding(b));

        //check when collision is not happening
        ScreenObject c = new ScreenObject(10, 10, new Dimension(100, 100));
        ScreenObject d = new ScreenObject(500, 500, new Dimension(100, 100));

        assertFalse(c.isColliding(d));
    }

}
