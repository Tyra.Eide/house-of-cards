package inf101v22.house_of_cards.screenObjects.levelDesign;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Dimension;
import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

/** Testing the class Ladder. */
public class LadderTest {

    @Test
    void LadderTestSanityCheck() {
        Ladder l = new Ladder(10, 10, new Dimension(10, 10));

        assertEquals(10, l.getX());
        assertEquals(10, l.getY());
        assertEquals(10, l.getDimension().width);
        assertEquals(10, l.getDimension().height);
        assertEquals(new Rectangle(15, 10, 2, 10), l.getHitbox());

    }
    
    @Test
    void LadderTestHitbox() {
        /**
         * Test that the hitbox of the ladder is a 2 pixel wide box at the middle of the ladder:
         * 1. in the draw() method of ladder, uncomment drawHitbox()
         * 2. run program
         * 3. press Start Game
         * 4. see that the hitbox looks correct on the ladders.
         */
    }
}
