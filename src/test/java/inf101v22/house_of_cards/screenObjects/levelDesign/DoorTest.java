package inf101v22.house_of_cards.screenObjects.levelDesign;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Dimension;
import java.awt.Rectangle;

import org.junit.jupiter.api.Test;

/** Testing of the class Door. */
public class DoorTest {
    
    Door d = new Door(10, 10, new Dimension(10, 10));

    @Test
    void DoorTestSanityCheck() {
        assertEquals(10, d.getX());
        assertEquals(10, d.getY());
        assertEquals(10, d.getDimension().width);
        assertEquals(10, d.getDimension().height);
        assertEquals(new Rectangle(10, 10, 10, 10), d.getHitbox());
    }
    
    @Test
    void DoorTestIsActive() {
        assertFalse(d.isActive());

        d.setActive(true);

        assertTrue(d.isActive());

    }
}
