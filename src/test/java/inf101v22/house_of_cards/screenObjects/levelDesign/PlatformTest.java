package inf101v22.house_of_cards.screenObjects.levelDesign;

import java.awt.Dimension;
import java.awt.Rectangle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/** Testing the class Platform. */
public class PlatformTest {
    
    @Test
    void PlatformTestSanityCheck() {
        Platform p = new Platform(10, 10, new Dimension(10, 10));

        assertEquals(10, p.getX());
        assertEquals(10, p.getY());
        assertEquals(10, p.getDimension().width);
        assertEquals(10, p.getDimension().height);
        assertEquals(new Rectangle(10, 10, 10, 10), p.getHitbox());

    }
}
