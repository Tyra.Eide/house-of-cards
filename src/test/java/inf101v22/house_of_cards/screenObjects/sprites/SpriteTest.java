package inf101v22.house_of_cards.screenObjects.sprites;

import java.awt.Dimension;
import java.awt.Rectangle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/** Testing of the class Sprite. */
public class SpriteTest {

    Sprite s = new Sprite(10, 10, 5, 6, "sprites/Player/Janus Girl Spritesheet.png");

    @Test
    void SpriteTestSanityCheck() {
        assertEquals(10, s.getX());
        assertEquals(10, s.getY());
        assertEquals(new Dimension(52, 64), s.getDimension());
        assertEquals(new Rectangle(10, 10, 52, 64), s.getHitbox());
    }

    @Test
    void SpriteTestCostumeTests() {
        assertEquals(30, s.getCostumeList().size());

        assertTrue(s.getCurrentCostume() == s.getCostumeList().get(0));

        s.setCurrentCostume(s.getCostumeList().get(1));

        assertTrue(s.getCurrentCostume() != s.getCostumeList().get(0));
    }

    @Test
    void SpriteTestSpeed() {
        s.setSpeed(10, 10);

        assertEquals(10, s.getSpeedx());
        assertEquals(10, s.getSpeedy());

        s.adjustSpeed(10, 10);

        assertEquals(20, s.getSpeedx());
        assertEquals(20, s.getSpeedy());
    }

    @Test
    void SpriteTestIsVisible() {
        assertTrue(s.isVisible());

        s.setVisible(false);

        assertFalse(s.isVisible());
    }

    @Test
    void SpriteTestCollisionAndRectionCheck() {
        Sprite a = new Sprite(0, 0, 5, 6, "sprites/Player/Janus Girl Spritesheet.png");
        Sprite b = new Sprite(52, 0, 5, 6, "sprites/Player/Janus Girl Spritesheet.png");

        //Horizontal collision
        a.setSpeed(10, 0);

        //check for collision
        assertTrue(a.checkHorizontalCollision(b));

        //see that speed was set to 0
        a.movePosition(a.getSpeedx(), a.getSpeedy());

        assertEquals(0, a.getX());

        //vertical collision
        b.setPosition(0, 64);
        b.setHitbox(new Rectangle(b.getX(), b.getY(), b.getDimension().width, b.getDimension().height));

        a.setSpeed(0, 10);

        //check for collison
        assertTrue(a.checkVerticalCollision(b));

        //see that speed was set to 0
        a.movePosition(a.getSpeedx(), a.getSpeedy());

        assertEquals(0, a.getY());

        //check when that there is no effect when no collision is detected.
        b.setPosition(100, 100);
        b.setHitbox(new Rectangle(b.getX(), b.getY(), b.getDimension().width, b.getDimension().height));

        a.setSpeed(10, 10);

        assertFalse(a.checkHorizontalCollision(b));
        assertFalse(a.checkVerticalCollision(b));

        a.movePosition(a.getSpeedx(), a.getSpeedy());
        assertEquals(10, a.getX());
        assertEquals(10, a.getY());


    }
}
