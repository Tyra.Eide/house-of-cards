package inf101v22.house_of_cards.screenObjects.sprites.projectiles;

import java.awt.Dimension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.ClubGuard;

/** Testing the Projectile class. */
public class ProjectileTest {
    
    @Test
    void ProjectileTestSanityCheck() {
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(10, 10, pc, 1);

        assertEquals(pc.getDamage(), p.getDamage());
        assertEquals(pc.getRange(), p.getRange());
        assertEquals(pc.getSpeed(), p.getSpeed());
    }

    @Test
    void ProjectileTestExhaustedCheck() {

        //check that when the range of the card is reached, the card becomes exhausted
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(10, 10, pc, 1);

        //see that when the range is reached, the projectile is set to exhausted the next time is moved
        for (int i = 0; i <= p.getRange() + p.getSpeed(); i += p.getSpeed()) {
            p.planMove();     
        }

        //see that the Projectile stops going forward
        assertTrue(p.isExhausted());
        assertEquals(0, p.getSpeedx());

        //see that Projectile falls down on the next move
        p.planMove();
        assertEquals(1, p.getSpeedy());
    }

    @Test
    void ProjectileTestHitOnImpact() {

        //check that the projectile is set to hit when hitting an obstacle
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(0, 0, pc, 1);

        Platform platform = new Platform(35, 0, new Dimension(5, 10));

        for (int i = 0; i < 2; i++) {
            p.planMove();
            p.checkHorizontalCollision(platform);
            p.movePosition(p.getSpeedx(), p.getSpeedy());
            p.moveHitbox(p.getSpeedx(), p.getSpeedy());
        }

        
        assertTrue(p.isHitting());
        assertEquals(35, p.getHitbox().x + p.getHitbox().width);
    }


    @Test
    void ProjectileTestExhaustedHit() {

        //Check that the Projectile hits when falling down as exhausted
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(0, 0, pc, 1);

        Platform platform = new Platform(52, 33, new Dimension(10, 5));


        for (int i = 0; i < p.getRange() + 15; i += p.getSpeed()) {
            p.planMove();
            p.checkVerticalCollision(platform);
            p.movePosition(p.getSpeedx(), p.getSpeedy());
            p.moveHitbox(p.getSpeedx(), p.getSpeedy());
        }

        assertTrue(p.isHitting());
    }

    @Test
    void ProjectileTestDealDamageOnHit() {
        //check that a Damageable takes damage upon being hit by a non-exhausted Projectible
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(0, 0, pc, 1);

        Monster monster = new ClubGuard(10, 0, 0, 0);

        for (int i = 0; i < 2; i++) {
            p.planMove();
            p.checkHit(monster);
            p.movePosition(p.getSpeedx(), p.getSpeedy());
            p.moveHitbox(p.getSpeedx(), p.getSpeedy());
        }

        assertTrue(p.isHitting());
        assertEquals(0, monster.getHealth());

        
    }

    @Test
    void ProjectileTestNoDamageOnExhaustedHit() {

        //check that Damageable does not take damage upon being hit by an exhausted Projectible
        Projectible pc = new PaperCard();
        Projectile p = new Projectile(0, 0, pc, 1);

        Monster monster = new ClubGuard(52, 33, 0, 0);


        for (int i = 0; i < p.getRange() + 15; i += p.getSpeed()) {
            p.planMove();
            p.checkHit(monster);
            p.movePosition(p.getSpeedx(), p.getSpeedy());
            p.moveHitbox(p.getSpeedx(), p.getSpeedy());
        }

        assertFalse(p.isHitting());
        assertEquals(ClubGuard.health, monster.getHealth());
    }

}
