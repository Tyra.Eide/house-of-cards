package inf101v22.house_of_cards.screenObjects.sprites.icons;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/** Testing the LifeHeart class. */
public class LifeHeartTest {
    
    @Test
    void LifeHeartTestDeathOnDepleatedHealth() {
        LifeHeart heart = new LifeHeart(0, 0);
        assertFalse(heart.isDead());

        //check that heart is set to dead when all health is depleated
        heart.decreaseHealth(100);

        assertTrue(heart.isDead());
        assertTrue(heart.isVisible());

        //check that heart is set invisible after death animation for 20 frames
        for (int i = 0; i < 22; i++) {
            heart.updateLifeHeart();
        }

        assertFalse(heart.isVisible());
    }

    /**
     * Testing costume animation:
     * 1. run program
     * 2. press start game
     * 3. when the first level loads, see that the heart is drawn as a full red heart in the top
     *      left corner of the game screen
     * 4. let the player walk onto a monster guard.
     * 5. see that the heart's content is decreased for each time the player takes damage
     * 6. see that, when the heart looses all health, it blinks for a few frames before disappearing
     */
}
