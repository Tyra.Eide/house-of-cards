package inf101v22.house_of_cards.screenObjects.sprites.player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Dimension;
import java.awt.Point;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;

/** Testing the Player class, and by exstension, the Sprite class and the PlayerCostumeManager class. */
public class PlayerTest {

    @Test
    void PlayerTestSanityTest() {
        Player player = new Player(20,100);
        player.setSpeed(3, 8);
        player.setVisible(false);

        //Position
        assertEquals(20, player.getX());
        assertEquals(100, player.getY());

        //Speed
        assertEquals(3, player.getSpeedx());
        assertEquals(8, player.getSpeedy());

        //Dimension
        assertEquals(new Dimension(52, 64), player.getDimension());


        //Visibility
        assertFalse(player.isVisible());


    }
    
    @Test
    void PlayerTestCostumeManager() {
        /**
         * Instructions:
         * 1. run the program
         * 2. go through all neccesary steps to start the game
         * 3. when the Player sprite appears on screen check the following:
         *      When using the right and left arrows, see that the sprite will alternate between the two running costumes for the given direction
         *      When standing on a platform, see that the sprite has the standing costume for the given direction it last moved in
         *      When jumping, see that the sprite goes through the cycle of charge, jump, fall, charge costumes for the direction last moved in or given by pressing the left and right arrow
         *      When attacking, see that the sprite has the attacking costume for the given direction
         *      When climbing, see that the sprite alternates between the two climbing costumes
         * 
         */
    }

    @Test
    void PlayerTestGetDirection() {
        Player player = new Player(0, 0);
        
        //move Player right
        player.planMove(false, true, false, false, false, false);
        assertEquals(1, player.getDirection());

        //move Player left
        player.planMove(true, false, false, false, false, false);
        assertEquals(-1, player.getDirection());
    }

    @Test
    void PlayerTestSetHitbox() {
        Player player = new Player(30, 30);

        //check that the hitbox is in the correct location for STANDING, RIGHT
        assertEquals(player.getX() + 23, player.getHitbox().x);
        assertEquals(player.getY() + 12, player.getHitbox().y);
        assertEquals(player.getDimension().width - 35, player.getHitbox().width);
        assertEquals(player.getDimension().height - 12, player.getHitbox().height);

        //check that the setHitbox() method sets the hibox to the correct position after having been misplaced
        player.moveHitbox(5, 5);
        player.setHitbox();
        assertEquals(player.getX() + 23, player.getHitbox().x);
    }

    @Test
    void PlayerTestMidpointBottom() {
        Player player = new Player(30, 30);

        //check that the midpoint of the bottom of the hitbox is initialized in the correct position
        assertEquals(new Point(61, 94), player.getMidpointBotom());

        //check that the midpoint remains correctly placed when the Player is moved and the hitbox is updated
        player.movePosition(5, 5);
        player.setHitbox();

        assertEquals(new Point(66, 99), player.getMidpointBotom());
    }

    @Test
    void PlayerTestMaxSpeed() {
        Player player = new Player(0, 0);

        //set the speed above max speed in both directions
        player.setSpeed(20, 39);

        //see that planMove() regulates the speed to not exceed to maximum
        player.planMove(false, true, false, false, false, false);

        assertEquals(5, player.getSpeedx());
        assertEquals(35, player.getSpeedy());
    }

    @Test
    void PlayerTestClassTest() {
        //This was purely for my own understanding
        ScreenObject player = new Player(0, 0);
        assertEquals(Player.class, player.getClass());
    }

    @Test
    void PlayerTestLadderCollisionCheck() {
        Player player = new Player(0, 0);
        Ladder ladder = new Ladder(20, 0, new Dimension(20, 100));

        //Ladder collision is not checked horizontally and vertically when standing still or moving down.
        assertFalse(player.checkHorizontalCollision(ladder));
        assertFalse(player.checkVerticalCollision(ladder));

        player.planMove(false, false, false, true, false, false);
        assertFalse(player.checkVerticalCollision(ladder));


        //Ladder collision is checked vertically when climbing up to make sure the player doesn't climb above the ladder
        player.planMove(false, false, true, false, false, false);
        assertTrue(player.checkVerticalCollision(ladder));
    }

    @Test
    void PlayerTestFallDamage() {
        //check that the player takes 25 damage when falling on a platform at a speed above 15 but below 20
        Player player1 = new Player(0, 0);
        Platform platform1 = new Platform(0, 79, new Dimension(100, 5));

        player1.setSpeed(0, 26);
        assertTrue(player1.checkVerticalCollision(platform1));
        assertEquals(275, player1.getHealth());

        //check that the player takes 50 damage when falling on a platform at a speed above 20 
        Player player2 = new Player(0, 0);
        Platform platform2 = new Platform(0, 79, new Dimension(100, 5));

        player2.setSpeed(0, 31);
        assertTrue(player2.checkVerticalCollision(platform2));
        assertEquals(250, player2.getHealth());

    }

    @Test
    void PlayerTestSlippingOffPlatform() {
        //Check that the player slips off the platform when landing with more than half of the hitbox off the edge
        Player player = new Player(0, 0);
        Platform platform = new Platform(0, 64, new Dimension(30, 5));

        //check that collision was not detected
        player.planMove(false, false, false, false, false, false);
        assertEquals(1, player.getSpeedy());

        //see that the midpoint is off the platform, but collision is detected
        assertTrue(!platform.getHitbox().contains(player.getMidpointBotom()));
        assertTrue(player.checkVerticalCollision(platform));

        //see that the hitbox was moved off the platform
        assertEquals(30, player.getHitbox().x);   
    }

    @Test
    public void PlayerTestUpdateLives() {
        Player player = new Player(0, 0);

        assertEquals(3, player.getLives().size());

        player.damaged(100, 0);
        
        for (int i = 0; i < 21; i++) {
            player.planMove(false, false, false, false, false, false);
            player.updateLives();
            assertEquals(0, player.getLives().get(player.getLives().size() - 1).getLeftOverDamage());
        }

        assertEquals(2, player.getLives().size());
        

        player.damaged(100, 0);
        
        for (int i = 0; i < 21; i++) {
            player.planMove(false, false, false, false, false, false);
            player.updateLives();
        }

        assertEquals(1, player.getLives().size());

        player.damaged(100, 0);
        
        for (int i = 0; i < 21; i++) {
            player.planMove(false, false, false, false, false, false);
            player.updateLives();
        }

        assertTrue(player.getLives().isEmpty());
    }


    //Things that have not been tested here, but by looking at the sprite behaviour on a low fram rate in-game:
    //Switching between and activating states
    //Platform collision detection skipped when player is intangible

}
