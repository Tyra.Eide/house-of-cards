package inf101v22.house_of_cards.screenObjects.sprites.monsters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.screenObjects.sprites.Damageable;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.ClubGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.DiamondGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.HeartGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.SpadeGuard;

/** Testing the Monster class. */
public class MonsterTest {

    @Test
    public void MonsterTestSanityCheck() {
        Monster guard = new ClubGuard(0, 0, -1, 100);

        //check that the monster stats matches the guard stats it was created from
        assertEquals(ClubGuard.damage, guard.damage);
        assertEquals(ClubGuard.speed, guard.speed);
        assertEquals(ClubGuard.health, guard.health);
        assertEquals(-2, guard.getSpeedx());
    }

    @Test
    public void MonsterTestCostumeAnimationCheck() {
        /**
         * Instructions:
         * 1. run the program
         * 2. press Start Game
         * 3. when a monster sprite appears on screen check the following:
         *      When the monster stands still, it alternates between two standing costumes
         *      When the monster walks, it alternates between two walking costumes
         *      When shooting at the monster and hitting it, the monster jumps into the air and blinks red before
         *      disappearing if it died or continuing to walk if it did not.
         */
    }

    @Test
    void MonsterTestTurnWhenWalking() {

        //check that the guard turns in the oposite direction when it has reached its range
        Monster guard = new DiamondGuard(0, 0, 1, 100);
        for (int i = 0; i <= guard.range + guard.speed; i += guard.speed) {
            guard.planMove();
        }

        assertEquals(-1, guard.direction);

        //check that the guard turns back again after walking the oposite direction
        for (int i = 0; i <= guard.range + guard.speed; i += guard.speed) {
            guard.planMove();
        }

        assertEquals(1, guard.direction);

    }

    @Test
    void MonsterTestStandingGuard() {

        //check that a guard with direction 0 does not move
        Monster guard = new HeartGuard(0, 0, 0, 0);

        guard.planMove();

        assertEquals(0, guard.getSpeedx());
        assertEquals(0, guard.getX());
        assertEquals(0, guard.getY());
    }
    
    @Test
    void MonsterTestTakeHit() {
        Monster guard = new SpadeGuard(10, 10, 1, 10);

        guard.damaged(100, 1);

        assertTrue(guard.isHit());
        assertEquals(300, guard.getHealth());

        //check that monster does not take damage when already in hit state
        guard.damaged(100, 1);
        assertEquals(300, guard.getHealth());

        //see that hit state only lasts 20 frames
        for (int i = 0; i < 22; i++) {
            guard.planMove();
        }

        assertFalse(guard.isHit());
    }

    @Test
    void MonsterTestDeath() {
        //check that guard dies when all health is depleated
        Monster guard = new HeartGuard(10, 10, 1, 10);

        guard.damaged(300, 1);

        //check that guard dies after damage animation (20 frames)
        for (int i = 0; i < 22; i++) {
            guard.planMove();
        }

        assertEquals(0, guard.health);
        assertTrue(guard.isDead());
    }

    @Test
    void MonsterTestCheckHit() {
        //check that monster registers hitting a Damagable

        Damageable c = new ClubGuard(32, 0, 1, 10);
        Monster g = new ClubGuard(0, 0, 1, 10);

        assertTrue(g.checkHit(c));

        assertEquals(75, c.getHealth());

    }
}
