package inf101v22.house_of_cards.menuSystem;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Dimension;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.model.HOCModel;

public class MenuTest {

    @Test
    public void MenuTestGetState() {
        String[] mainMenuButtons = {"START GAME", "EXIT"};
        GameState[] mainMenuStates = {GameState.ACTIVE, GameState.CLOSE};

        Menu menu = new Menu(new Dimension(HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height), mainMenuButtons, mainMenuStates, "House of Cards");

        assertEquals(2, menu.getButtons().size());

        assertEquals(GameState.ACTIVE, menu.getButtons().get(0).getState());
        assertEquals(GameState.CLOSE, menu.getButtons().get(1).getState());
        

    }
}
