package inf101v22.house_of_cards.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Dimension;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.levelControl.LevelCategory;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.player.Player;

/** Testing of the class HOCModel. */
public class HOCModelTest {
    
    @Test
    void HOCModelTestSanityCheck() {

        //Player
        Player player = new Player(400, 250);

        //Platforms
        Platform platform1 = new Platform(120, 410, new Dimension(30, 5));
        Platform platform2 = new Platform(150, 380, new Dimension(30, 5));
        Platform platform3 = new Platform(0, 463, new Dimension(500, 1));

        ArrayList<Platform> platforms = new ArrayList<>();
        platforms.add(platform1);
        platforms.add(platform2);
        platforms.add(platform3);

        Ladder ladder1 = new Ladder(30, 10, new Dimension(40, 200));
        Ladder ladder2 = new Ladder(200, 420, new Dimension(40, 100));

        ArrayList<Ladder> ladders = new ArrayList<>();
        ladders.add(ladder1);
        ladders.add(ladder2);

        
        HOCModel model = new HOCModel(player, platforms, ladders, null);

        assertEquals(player, model.getPlayer());
        assertEquals(platforms, model.getPlatforms());
        assertEquals(ladders, model.getLadders());
    }

    @Test
    void HOCModelTestStartGame() {
        HOCModel model = new HOCModel();
        model.startGame();

        assertEquals(GameState.NEXT_LEVEL, model.getState());
        assertEquals(1, model.getLevelNumber());
        assertEquals(LevelCategory.CLUBS, model.getLevelCategory());
    }

    @Test
    void HOCModelTestPlayerStandingOnPlatform() {

        //check that the model succesfully informs the player if it is standing on a platform
        Player player1 = new Player(0, 0);
        Platform platform1 = new Platform(0, 64, new Dimension(100, 5));
        
        ArrayList<Platform> platforms1 = new ArrayList<>();
        platforms1.add(platform1);

        HOCModel model1 = new HOCModel(player1, platforms1, null, null);

        model1.updateModel(false, false, false, false, false, false);
        assertTrue(model1.getPlayer().isStandingOnPlatform());

        //check that the model does not tell the player it is standing on a platform if it is not
        Player player2 = new Player(0, 0);
        Platform platform2 = new Platform(0, 100, new Dimension(100, 5));
        
        ArrayList<Platform> platforms2 = new ArrayList<>();
        platforms2.add(platform2);

        HOCModel model2 = new HOCModel(player2, platforms2, null, null);

        model2.updateModel(false, false, false, false, false, false);
        assertFalse(model2.getPlayer().isStandingOnPlatform());
    }

    @Test
    void HOCModelTestPlayerIsIntangible() {

        //check that the model successfully tells the player that it is intangible when being in a ladder above the middle of it

        Player player1 = new Player(0, 0);
        Ladder ladder1 = new Ladder(3, 0, new Dimension(40, 200));

        ArrayList<Ladder> ladders1 = new ArrayList<>();
        ladders1.add(ladder1);

        HOCModel model1 = new HOCModel(player1, null, ladders1, null);

        model1.updateModel(false, false, false, false, false, false);
        assertTrue(model1.getPlayer().isIntangible());


        //check that the model does not tell the player that it is intangible when being in a ladder below the middle of it

        Player player2 = new Player(0, 50);
        Ladder ladder2 = new Ladder(3, 0, new Dimension(40, 200));

        ArrayList<Ladder> ladders2 = new ArrayList<>();
        ladders2.add(ladder2);

        HOCModel model2 = new HOCModel(player2, null, ladders2, null);

        model2.updateModel(false, false, false, false, false, false);
        assertFalse(model2.getPlayer().isIntangible());
    }

    @Test
    void HOCModelTestPlayerCanAttack() {

        //check that the player is allowed to attack when the cooldown has finished and the player is standing on a platform
        Player player = new Player(0, 0);
        Platform platform = new Platform(0, 64, new Dimension(100, 5));
        
        ArrayList<Platform> platforms = new ArrayList<>();
        platforms.add(platform);
        

        HOCModel model = new HOCModel(player, platforms, null, null);

        model.updateModel(false, false, false, false, false, true);
        assertTrue(player.isAttacking());

        //check that the player is not allowed to attack when the cooldown has not finished

        //let the player attack from before wear off
        for (int frames = 0; frames < 6; frames++) {
            model.updateModel(false, false, false, false, false, true);
        }
        
        assertFalse(model.getPlayer().isAttacking());

    }

    @Test
    void HOCModelTestDeadMonstersExhaustedProjectiles() {
        /**
         * How to test that monsters and projectiles are removed from the model when dead/exhausted:
         * 1. Start the program
         * 2. Click Start Game
         * 3. When the first level loads, walk over to a guard
         * 4. press A to send a card flying towards the guard
         * 5. See that when the card hits the guard, the guard disappears when being hit enough times to depleat all its health
         *      If the test is performed on level 1, this should only be one time
         * 6. See that the card also disappears after having hit the monster
         * 7. See that if shooting again, into the air, the card will still disappear when it exhausts its range and falls down
         */
    }

    @Test
    void HOCModelTestNextLevel() {
        /**
         * How to test that the model goes to the next level when the player walks through the door:
         * 1. run the program
         * 2. click start game
         * 3. when the first level loads, play through the level until the player reaches the door
         * 4. step into the door, and see that the level screen for the next level is shown
         * 5. see that the next level is loaded after the level screen has been shown
         */
    }

    @Test
    void HOCModelSetMenu() {
        /**
         * How to check that the model sets the correct menu for its state:
         * 1. Run the program
         * 2. See that the menu that pops up has the title "House of Cards" and the buttons "Start Game" and "Exit".
         * 3. Click Start game
         * 4. When the first level loads, press P
         * 5. See that the menu that pops up has the title "Paused" and the buttons "Continue", "Exit to Main Menu" and "Exit to Desktop"
         * 6. See that the game goes back to the main menu when pressing "Exit to Main Menu"
         * 7. Do step 3 again, and play the game, this time making sure that the player dies
         * 8. See that the menu that pops up is named "Game Over" and that the buttons are "Replay", and "Main Menu"
         * 9. See that the game goes back to the main menu when pressing "Main Menu"
         * 10. Do step 3 again, and play the game, this time succeeding all the levels
         * 11. See that the menu that pops up has the title "To be continued" and the buttons "Replay" and "Main Menu"
         */
    }



}