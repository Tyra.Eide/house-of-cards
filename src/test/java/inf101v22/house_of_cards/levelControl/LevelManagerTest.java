package inf101v22.house_of_cards.levelControl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.model.HOCModel;

/** Testing the LevelManager class. */
public class LevelManagerTest {
    
    @Test
    public void LevelManagerTestInitializeLevels() {
        HOCModel model = new HOCModel(null, null, null, null);
        LevelManager levelManager = new LevelManager(model);

        levelManager.startGame();

        assertEquals(13, levelManager.getAllLevelsInfo().size());

    }

    @Test
    public void LevelManagerTestLoadLevel() {
        HOCModel model = new HOCModel(null, null, null, null);
        LevelManager levelManager = new LevelManager(model);

        levelManager.startGame();

        ILevel currentLevel = levelManager.getCurrentLevel();

        assertEquals(6, currentLevel.getPlatforms().size());
        assertEquals(2, currentLevel.getLadders().size());
        assertEquals(0, currentLevel.getMonsters().size());
        assertEquals(LevelCategory.CLUBS, currentLevel.getLevelCategory());
        assertEquals(1, currentLevel.getLevelNumber());

        assertEquals(model.getPlatforms(), levelManager.getCurrentLevel().getPlatforms());
    }

    @Test
    public void LevelManagerTestNextLevel() {
        HOCModel model = new HOCModel(null, null, null, null);
        LevelManager levelManager = new LevelManager(model);

        levelManager.startGame();

        assertEquals(1, levelManager.getCurrentLevel().getLevelNumber());

        levelManager.nextLevel();

        assertEquals(2, levelManager.getCurrentLevel().getLevelNumber());
    }
}
