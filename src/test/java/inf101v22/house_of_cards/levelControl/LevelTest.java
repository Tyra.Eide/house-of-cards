package inf101v22.house_of_cards.levelControl;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.SpadeGuard;

public class LevelTest {
    
    @Test
    public void levelTestSanityCheck() {
        int[][] testLevelPlatforms = new int[][]{{100, 300, 50}, {200, 400, 10}, {100, 100, 400}, {5, 5, 70}};
        int[][] testLevelLadders = new int[][]{{350, 120}, {210, 300}, {40, 20}, {100, 430}};
        int[][] testLevelMonsters = new int[][]{{350}, {146}, {1}, {100}};

        Level testLevel = new Level(LevelCategory.SPADES, 5, testLevelPlatforms, testLevelLadders, testLevelMonsters, null);

        assertEquals(7, testLevel.getPlatforms().size());
        assertEquals(300 + HOCModel.getOffset(), testLevel.getPlatforms().get(1).getX());

        assertEquals(2, testLevel.getLadders().size());
        assertEquals(430, testLevel.getLadders().get(1).getDimension().height);

        assertEquals(1, testLevel.getMonsters().size());
        assertTrue(testLevel.getMonsters().get(0).getSpeedx() > 0);
        assertTrue(testLevel.getMonsters().get(0).getSpeedy() == 0);
        assertEquals(SpadeGuard.class, testLevel.getMonsters().get(0).getClass());

        assertEquals(5, testLevel.getLevelNumber());
        assertEquals(LevelCategory.SPADES, testLevel.getLevelCategory());
    }
    


}
