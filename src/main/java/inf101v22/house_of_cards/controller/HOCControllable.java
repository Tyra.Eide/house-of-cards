package inf101v22.house_of_cards.controller;

import inf101v22.house_of_cards.menuSystem.Menu;
import inf101v22.house_of_cards.model.GameState;

/**
 * Methods needed to control a game of House of Cards.
 * This involves updating the game with instructions for moving the player, getting and setting the {@link GameState} of the game
 * setting the {@link Menu} of the game and starting the game.
 * @author Tyra Fosheim Eide
 */
public interface HOCControllable {
    
    /**
     * Sets the {@link GameState} of the model to a spesified state.
     * @param state the specified state.
     */
    public void setState(GameState state);

    /** @return the {@link GameState} of the model. */
    public GameState getState();

    /**
     * Update the player, projectiles, door and monsters of the model.
     * Checks all for collision and acts accordingly, and moves sprites
     * according to their path.
     * @param left instruction for {@link Player} movement
     * @param right instruction for {@link Player} movement     
     * @param up instruction for {@link Player} movement
     * @param down instruction for {@link Player} movement
     * @param jump instruction for {@link Player} movement

     * @param attack instruction for {@link Player} movement
     */
    public void updateModel(boolean left, boolean right, boolean up, boolean down, boolean jump, boolean attack);

    /** Sets the menu of the model according to model state. */
    public void setMenu();

    /** Gets the current menu of the model. */
    public Menu getMenu();

    /** Starts a new game of House of Cards. */
    public void startGame();


}
