package inf101v22.house_of_cards.controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.Timer;
import javax.swing.event.MouseInputListener;

import inf101v22.house_of_cards.HOCMain;
import inf101v22.house_of_cards.menuSystem.Button;
import inf101v22.house_of_cards.model.GameState;

/**
 * Controller of a game of House of Cards.
 * Listens to all user input such as keypresses and mouse input, as well as
 * keeping track of the timer of the game.
 * @author Tyra Fosheim Eide
 */
public class HOCController implements KeyListener, ActionListener, MouseInputListener {
    private HOCControllable hocModel;
    private JComponent hocView;
    private Timer timer;

    private int timeCounter;

    private boolean left;
    private boolean right;
    private boolean up;
    private boolean down;
    private boolean jump;
    private boolean attack;

    public HOCController(HOCControllable hocModel, JComponent hocView) {
        this.hocModel = hocModel;
        this.hocView = hocView;
        timer = new Timer(17, this);

        timeCounter = 0;

        this.hocView.addKeyListener(this);
        this.hocView.addMouseListener(this);
        this.hocView.addMouseMotionListener(this);
    
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (hocModel.getState() == GameState.ACTIVE) {
            hocModel.updateModel(left, right, up, down, jump, attack);
        }

        else if (hocModel.getState() == GameState.GAME_OVER) {
            timer.stop();
        }

        //timed introduction screen
        else if (hocModel.getState() == GameState.START) {
            if (timeCounter < 400) {
                timeCounter++;
                return;
            }

            timeCounter = 0;
            hocModel.startGame();
        }

        //timed next level screen
        else if (hocModel.getState() == GameState.NEXT_LEVEL) {
            if (timeCounter < 90) {
                timeCounter++;
                return;
            }

            timeCounter = 0;
            hocModel.setState(GameState.ACTIVE);
        }

        //update the view
        hocView.repaint();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (hocModel.getState() == GameState.ACTIVE) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                left = true;
            }

            if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
                right = true;
            }

            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                jump = true;
            }

            if (e.getKeyCode() == KeyEvent.VK_A) {
                attack = true;
            }

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                up = true;
            }

            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                down = true;
            }

            if (e.getKeyCode() == KeyEvent.VK_P) {
                hocModel.setState(GameState.PAUSED);
                timer.stop();
                hocModel.setMenu();
                hocView.repaint();
            }
        }

        else if (hocModel.getState() == GameState.PAUSED) {
            if (e.getKeyCode() == KeyEvent.VK_P) {
                hocModel.setState(GameState.ACTIVE);
                timer.start();
            }
        }

        else if (hocModel.getState() == GameState.START) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                timeCounter = 400;
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            this.left = false;
        }

        if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
            this.right = false;
        }

        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            this.jump = false;
        }

        if (e.getKeyCode() == KeyEvent.VK_A) {
            this.attack = false;
        }

        if (e.getKeyCode() == KeyEvent.VK_UP) {
            this.up = false;
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            this.down = false;
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {
        for (Button button : hocModel.getMenu().getButtons()) {
            if (button.getHitbox().contains(e.getLocationOnScreen())) {
                button.setMouseClicked(true);
                break;
            }
        }

        hocView.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for (Button button : hocModel.getMenu().getButtons()) {
            if (button.getHitbox().contains(e.getLocationOnScreen())) {
                hocModel.setState(button.getState());
                break;
            }
        }

        hocModel.getMenu().resetButtons();

        if (hocModel.getState() == GameState.ACTIVE || hocModel.getState() == GameState.START) {
            timer.start();
        }

        if (hocModel.getState() == GameState.CLOSE) {
            HOCMain.closeWindow();
        }

        hocModel.setMenu();
        hocView.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}


    @Override
    public void mouseMoved(MouseEvent e) {
        for (Button button : hocModel.getMenu().getButtons()) {
            button.setMouseOver(false);
        }
        
        for (Button button : hocModel.getMenu().getButtons()) {
            if (button.getHitbox().contains(e.getLocationOnScreen())) {
                button.setMouseOver(true);
                break;
            }
        }

        hocView.repaint();
    }

}
