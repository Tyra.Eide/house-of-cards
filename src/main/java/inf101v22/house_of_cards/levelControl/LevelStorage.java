package inf101v22.house_of_cards.levelControl;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Level storage of House of Cards.
 * Stores information about all levels, and makes it possible to access the information.
 * @author Tyra Fosheim Eide
 */
public class LevelStorage {
    
    /**
     * A storage unit, storing the category and number of a level as well as information about placement of the levels
     * platforms, ladders, monsters and door.
     * @author Tyra Fosheim Eide
     */
    static class LevelStorer {
        protected LevelCategory category;
        protected int number;
        protected int[][] platforms;
        protected int[][] ladders;
        protected int[][] monsters;
        protected int[] door;

        /**
         * Creates a storage unit for a set of level information
         * @param category {@link LevelCategory} of the level
         * @param number the number within the level category of the level
         * @param platforms two dimentional array of ints containing x-coordinates, y-coordinates, width and height of all the level's {@link Platform}s
         * @param ladders two dimentional array of ints containing x-coordinates, y-coordinates, width and height of all the level's {@link Ladder}s
         * @param monsters two dimentional array of ints containing x-coordinates, y-coordinates, direction and range of all the level's {@link Monster}s
         * @param door array of ints specifying the x-coordinate, y-coordinate, width and height if the level's {@link Door}
         */
        private LevelStorer(LevelCategory category, int number, int[][] platforms, int[][] ladders, int[][] monsters, int[] door) {
            this.category = category;
            this.number = number;
            this.platforms = platforms;
            this. ladders = ladders;
            this.monsters = monsters;
            this.door = door;
        }
    }

    /** @return a list of storage units ({@link LevelStorer}s) containing information about all levels of House of Cards. */
    protected static ArrayList<LevelStorer> loadLevels() {
            
        //---------------------------- C L U B S -------------------------------------------

        //LEVEL 1
        int[] c1PlatformsX = new int[]{200, 200};
        int[] c1PlatformsY = new int[]{500, 200};
        int[] c1PlatformsWidth = new int[]{400, 500};
        int[] c1PlatformsHeight = new int[]{5, 5};

        int[][] levelC1Platforms = new int[][]{c1PlatformsX, c1PlatformsY, c1PlatformsWidth, c1PlatformsHeight};

        int[] c1LaddersX = new int[]{575, 200};
        int[] c1LaddersY = new int[]{500, 200};
        int[] c1LaddersWidth = new int[]{25, 25};
        int[] c1LaddersHeight = new int[]{362, 300};

        int[][] levelC1Ladders = new int[][]{c1LaddersX, c1LaddersY, c1LaddersWidth, c1LaddersHeight};

        int[] c1MonstersX = new int[]{};
        int[] c1MonsterY = new int[]{};
        int[] c1MonstersDirection = new int[]{};
        int[] c1MonstersRange = new int[]{};

        int[][] levelC1Monsters = new int[][]{c1MonstersX, c1MonsterY, c1MonstersDirection, c1MonstersRange};

        int[] levelC1Door = new int[]{650, 136, 50, 64};

        LevelStorer levelC1 = new LevelStorer(LevelCategory.CLUBS, 1, levelC1Platforms, levelC1Ladders, levelC1Monsters, levelC1Door);
        


        //LEVEL 2
        int[] c2PX = new int[]{100, 410, 475};
        int[] c2PY = new int[]{400, 400, 200};
        int[] c2PW = new int[]{200, 200, 200};
        int[]c2PH = new int[]{5, 5, 5};

        int[][] levelC2Platforms = new int[][]{c2PX, c2PY, c2PW, c2PH};

        int[] c2LX = new int[]{150, 525};
        int[] c2LY = new int[]{400, 200};
        int[] c2LW = new int[]{25, 25};
        int[] c2LH = new int[]{462, 200};

        int[][] levelC2Ladders = new int[][]{c2LX, c2LY, c2LW, c2LH};

        int[] c2MX = new int[]{};
        int[] c2MY = new int[]{};
        int[] c2MD = new int[]{};
        int[] c2MR = new int[]{};

        int[][] levelC2Monsters = new int[][]{c2MX, c2MY, c2MD, c2MR};

        int[] levelC2Door = new int[]{625, 136, 50, 64};

        LevelStorer levelC2 = new LevelStorer(LevelCategory.CLUBS, 2, levelC2Platforms, levelC2Ladders, levelC2Monsters, levelC2Door);



        //LEVEL 3
        int[] c3PX = new int[]{300, 100, 300, 525};
        int[] c3PY = new int[]{600, 500, 425, 200};
        int[] c3PW = new int[]{400, 150, 300, 275};
        int[] c3PH = new int[]{5, 5, 5, 5};

        int[][] levelC3Platforms = new int[][]{c3PX, c3PY, c3PW, c3PH};

        int[] c3LX = new int[]{350, 550};
        int[] c3LY = new int[]{600, 200};
        int[] c3LW = new int[]{25, 25};
        int[] c3LH = new int[]{262, 225};

        int[][] levelC3Ladders = new int[][]{c3LX, c3LY, c3LW, c3LH};

        int[] c3MX = new int[]{};
        int[] c3MY = new int[]{};
        int[] c3MD = new int[]{};
        int[] c3MR = new int[]{};

        int[][] levelC3Monsters = new int[][]{c3MX, c3MY, c3MD, c3MR};

        int[] levelC3Door = new int[]{748, 136, 50, 64};

        LevelStorer levelC3 = new LevelStorer(LevelCategory.CLUBS, 3, levelC3Platforms, levelC3Ladders, levelC3Monsters, levelC3Door);



        //LEVEL 4
        int[] c4PX = new int[]{475, 350, 100, 100};
        int[] c4PY = new int[]{600, 600, 600, 200};
        int[] c4PW = new int[]{125, 64, 150, 250};
        int[] c4PH = new int[]{5, 5, 5, 5};

        int[][] levelC4Platforms = new int[][]{c4PX, c4PY, c4PW, c4PH};

        int[] c4LX = new int[]{525, 125};
        int[] c4LY = new int[]{600, 200};
        int[] c4LW = new int[]{25, 25};
        int[] c4LH = new int[]{262, 400};

        int[][] levelC4Ladders = new int[][]{c4LX, c4LY, c4LW, c4LH};

        int[] c4MX = new int[]{350};
        int[] c4MY = new int[]{536};
        int[] c4MD = new int[]{0};
        int[] c4MR = new int[]{0};

        int[][] levelC4Monsters = new int[][]{c4MX, c4MY, c4MD, c4MR};

        int[] levelC4Door = new int[]{200, 136, 50, 64};

        LevelStorer levelC4 = new LevelStorer(LevelCategory.CLUBS, 4, levelC4Platforms, levelC4Ladders, levelC4Monsters, levelC4Door);



        //LEVEL 5
        int[] c5PX = new int[]{100, 350, 350, 100};
        int[] c5PY = new int[]{775, 680, 300, 250};
        int[] c5PW = new int[]{200, 350, 350, 140};
        int[] c5PH = new int[]{5, 5, 5, 5};

        int[][] levelC5Platforms = new int[][]{c5PX, c5PY, c5PW, c5PH};

        int[] c5LX = new int[]{675};
        int[] c5LY = new int[]{300};
        int[] c5LW = new int[]{25};
        int[] c5LH = new int[]{380};

        int[][] levelC5Ladders = new int[][]{c5LX, c5LY, c5LW, c5LH};

        int[] c5MX = new int[]{230, 400};
        int[] c5MY = new int[]{711, 236};
        int[] c5MD = new int[]{0, 0};
        int[] c5MR = new int[]{0, 0};

        int[][] levelC5Monsters = new int[][]{c5MX, c5MY, c5MD, c5MR};

        int[] levelC5Door = new int[]{100, 186, 50, 64};

        LevelStorer levelC5 = new LevelStorer(LevelCategory.CLUBS, 5, levelC5Platforms, levelC5Ladders, levelC5Monsters, levelC5Door);



        //LEVEL 6
        int[] c6PX = new int[]{200, 200};
        int[] c6PY = new int[]{500, 200};
        int[] c6PW = new int[]{400, 500};
        int[] c6PH = new int[]{5, 5};

        int[][] levelC6Platforms = new int[][]{c6PX, c6PY, c6PW, c6PH};

        int[] c6LX = new int[]{575, 200};
        int[] c6LY = new int[]{500, 200};
        int[] c6LW = new int[]{25, 25};
        int[] c6LH = new int[]{362, 300};

        int[][] levelC6Ladders = new int[][]{c6LX, c6LY, c6LW, c6LH};

        int[] c6MX = new int[]{525};
        int[] c6MY = new int[]{436};
        int[] c6MD = new int[]{-1};
        int[] c6MR = new int[]{175};

        int[][] levelC6Monsters = new int[][]{c6MX, c6MY, c6MD, c6MR};

        int[] levelC6Door = new int[]{650, 136, 50, 64};

        LevelStorer levelC6 = new LevelStorer(LevelCategory.CLUBS, 6, levelC6Platforms, levelC6Ladders, levelC6Monsters, levelC6Door);



        //LEVEL 7
        int[] c7PX = new int[]{350, 100, 100};
        int[] c7PY = new int[]{500, 400, 200};
        int[] c7PW = new int[]{300, 200, 200};
        int[] c7PH = new int[]{5, 5, 5};

        int[][] levelC7Platforms = new int[][]{c7PX, c7PY, c7PW, c7PH};

        int[] c7LX = new int[]{600, 100};
        int[] c7LY = new int[]{500, 200};
        int[] c7LW = new int[]{25, 25};
        int[] c7LH = new int[]{362, 200};

        int[][] levelC7Ladders = new int[][]{c7LX, c7LY, c7LW, c7LH};

        int[] c7MX = new int[]{565, 236};
        int[] c7MY = new int[]{436, 336};
        int[] c7MD = new int[]{-1, -1};
        int[] c7MR = new int[]{186, 136};

        int[][] levelC7Monsters = new int[][]{c7MX, c7MY, c7MD, c7MR};

        int[] levelC7Door = new int[]{200, 136, 50, 64};

        LevelStorer levelC7 = new LevelStorer(LevelCategory.CLUBS, 7, levelC7Platforms, levelC7Ladders, levelC7Monsters, levelC7Door);



        //LEVEL 8
        int[] c8PX = new int[]{100, 260, 400, 560, 390, 200, 100};
        int[] c8PY = new int[]{800, 700, 600, 500, 400, 400, 300};
        int[] c8PW = new int[]{100, 100, 100, 100, 100, 125, 100};
        int[] c8PH = new int[]{5, 5, 5, 5, 5, 5, 5,};

        int[][] levelC8Platforms = new int[][]{c8PX, c8PY, c8PW, c8PH};

        int[] c8LX = new int[]{};
        int[] c8LY = new int[]{};
        int[] c8LW = new int[]{};
        int[] c8LH = new int[]{};

        int[][] levelC8Ladders = new int[][]{c8LX, c8LY, c8LW, c8LH};

        int[] c8MX = new int[]{386, 256};
        int[] c8MY = new int[]{336, 336};
        int[] c8MD = new int[]{0, 0};
        int[] c8MR = new int[]{0, 0};

        int[][] levelC8Monsters = new int[][]{c8MX, c8MY, c8MD, c8MR};

        int[] levelC8Door = new int[]{100, 236, 50, 64};

        LevelStorer levelC8 = new LevelStorer(LevelCategory.CLUBS, 8, levelC8Platforms, levelC8Ladders, levelC8Monsters, levelC8Door);

        

        //LEVEL 9
        int[] c9PX = new int[]{300, 100, 100};
        int[] c9PY = new int[]{400, 400, 200};
        int[] c9PW = new int[]{300, 150, 100};
        int[] c9PH = new int[]{5, 5, 5};

        int[][] levelC9Platforms = new int[][]{c9PX, c9PY, c9PW, c9PH};

        int[] c9LX = new int[]{500, 100};
        int[] c9LY = new int[]{400, 200};
        int[] c9LW = new int[]{25, 25};
        int[] c9LH = new int[]{462, 200};

        int[][] levelC9Ladders = new int[][]{c9LX, c9LY, c9LW, c9LH};

        int[] c9MX = new int[]{200, 300, 200};
        int[] c9MY = new int[]{798, 336, 336};
        int[] c9MD = new int[]{1, 1, 0};
        int[] c9MR = new int[]{400, 236, 0};

        int[][] levelC9Monsters = new int[][]{c9MX, c9MY, c9MD, c9MR};

        int[] levelC9Door = new int[]{150, 136, 50, 64};

        LevelStorer levelC9 = new LevelStorer(LevelCategory.CLUBS, 9, levelC9Platforms, levelC9Ladders, levelC9Monsters, levelC9Door);



        //LEVEL 10
        int[] c10PX = new int[]{200, 400, 100, 100};
        int[] c10PY = new int[]{700, 450, 400, 200};
        int[] c10PW = new int[]{400, 200, 200, 600};
        int[] c10PH = new int[]{5, 5, 5, 5};

        int[][] levelC10Platforms = new int[][]{c10PX, c10PY, c10PW, c10PH};

        int[] c10LX = new int[]{200, 575, 200};
        int[] c10LY = new int[]{700, 450, 200};
        int[] c10LW = new int[]{25, 25, 25};
        int[] c10LH = new int[]{162, 250, 200};

        int[][] levelC10Ladders = new int[][]{c10LX, c10LY, c10LW, c10LH};

        int[] c10MX = new int[]{200, 536, 600};
        int[] c10MY = new int[]{636, 636, 136};
        int[] c10MD = new int[]{1, -1, -1};
        int[] c10MR = new int[]{336, 336, 400};

        int[][] levelC10Monsters = new int[][]{c10MX, c10MY, c10MD, c10MR};

        int[] levelC10Door = new int[]{650, 136, 50, 64};

        LevelStorer levelC10 = new LevelStorer(LevelCategory.CLUBS, 10, levelC10Platforms, levelC10Ladders, levelC10Monsters, levelC10Door);



        //LEVEL 11
        int[] c11PX = new int[]{350, 200, 360, 300, 50};
        int[] c11PY = new int[]{760, 700, 600, 300, 300};
        int[] c11PW = new int[]{150, 100, 240, 300, 125};
        int[] c11PH = new int[]{5, 5, 5, 5, 5};

        int[][] levelC11Platforms = new int[][]{c11PX, c11PY, c11PW, c11PH};

        int[] c11LX = new int[]{575};
        int[] c11LY = new int[]{300};
        int[] c11LW = new int[]{25};
        int[] c11LH = new int[]{300};

        int[][] levelC11Ladders = new int[][]{c11LX, c11LY, c11LW, c11LH};

        int[] c11MX = new int[]{500, 200, 550, 550};
        int[] c11MY = new int[]{798, 636, 536, 236};
        int[] c11MD = new int[]{-1, 0, -1, -1};
        int[] c11MR = new int[]{300, 0,  170, 260};

        int[][] levelC11Monsters = new int[][]{c11MX, c11MY, c11MD, c11MR};

        int[] levelC11Door = new int[]{50, 236, 50, 64};

        LevelStorer levelC11 = new LevelStorer(LevelCategory.CLUBS, 11, levelC11Platforms, levelC11Ladders, levelC11Monsters, levelC11Door);



        //LEVEL 12
        int[] c12PX = new int[]{200, 200, 400, 600};
        int[] c12PY = new int[]{700, 500, 350, 200};
        int[] c12PW = new int[]{300, 225, 225, 100};
        int[] c12PH = new int[]{5, 5, 5, 5};

        int[][] levelC12Platforms = new int[][]{c12PX, c12PY, c12PW, c12PH};

        int[] c12LX = new int[]{425, 200, 400, 600};
        int[] c12LY = new int[]{700, 500, 350, 200};
        int[] c12LW = new int[]{25, 25, 25, 25};
        int[] c12LH = new int[]{162, 200, 150, 150};

        int[][] levelC12Ladders = new int[][]{c12LX, c12LY, c12LW, c12LH};

        int[] c12MX = new int[]{430, 200, 200, 316, 500};
        int[] c12MY = new int[]{636, 636, 436, 436, 286};
        int[] c12MD = new int[]{-1, 1, 1, -1, 0};
        int[] c12MR = new int[]{256, 256, 81, 81, 0};

        int[][] levelC12Monsters = new int[][]{c12MX, c12MY, c12MD, c12MR};

        int[] levelC12Door = new int[]{650, 136, 50, 64};

        LevelStorer levelC12 = new LevelStorer(LevelCategory.CLUBS, 12, levelC12Platforms, levelC12Ladders, levelC12Monsters, levelC12Door);



        //LEVEL 13
        int[] c13PX = new int[]{100, 400, 200, 300};
        int[] c13PY = new int[]{500, 400, 300, 200};
        int[] c13PW = new int[]{600, 200, 200, 200};
        int[] c13PH = new int[]{5, 5, 5, 5};

        int[][] levelC13Platforms = new int[][]{c13PX, c13PY, c13PW, c13PH};

        int[] c13LX = new int[]{350};
        int[] c13LY = new int[]{500};
        int[] c13LW = new int[]{25};
        int[] c13LH = new int[]{364};

        int[][] levelC13Ladders = new int[][]{c13LX, c13LY, c13LW, c13LH};

        int[] c13MX = new int[]{200, 500, 280, 416, 500, 200};
        int[] c13MY = new int[]{798, 798, 436, 436, 336, 236};
        int[] c13MD = new int[]{1, -1, 1, -1, 0, 1};
        int[] c13MR = new int[]{300, 300, 200, 200, 0, 150};

        int[][] levelC13Monsters = new int[][]{c13MX, c13MY, c13MD, c13MR};

        int[] levelC13Door = new int[]{400, 136, 50, 64};

        LevelStorer levelC13 = new LevelStorer(LevelCategory.CLUBS, 13, levelC13Platforms, levelC13Ladders, levelC13Monsters, levelC13Door);


        //CLUB LEVELS
        LevelStorer[] clubLevels = new LevelStorer[]{levelC1, levelC2, levelC3, levelC4, levelC5, levelC6, levelC7, levelC8, levelC9, levelC10, levelC11, levelC12, levelC13};

        //-------------------------------- S U M M A R Y ------------------------------------

        ArrayList<LevelStorer> allLevels = new ArrayList<>();
        allLevels.addAll(Arrays.asList(clubLevels));
        
        return allLevels;
    }
}
