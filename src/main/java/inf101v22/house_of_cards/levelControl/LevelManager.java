package inf101v22.house_of_cards.levelControl;

import java.util.ArrayList;

import inf101v22.house_of_cards.levelControl.LevelStorage.LevelStorer;
import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.model.HOCModel;

/**
 * Level manager for a game of House of Cards.
 * Keeps track of what level the game is on and feeds the level into the game.
 * @author Tyra Fosheim Eide
 */
public class LevelManager {
    private ArrayList<LevelStorer> allLevelsInfo;

    private ILevel currentLevel;
    private int levelNumber;

    private HOCModel model;

    /**
     * Creates a {@code LevelManager} for a {@link HOCModel}
     * @param model the {@link HOCModel} to manage levels for
     */
    public LevelManager(HOCModel model) {
        this.model = model;
    }

    /** Loads all level information into the manager. */
    private void initializeLevels() {
        allLevelsInfo = LevelStorage.loadLevels();
    }
    
    /** Initializes levels and feeds the first level into the game. */
    public void startGame() {
        model.setState(GameState.NEXT_LEVEL);
        initializeLevels();
        levelNumber = 0;
        loadLevel();
    }

    /**
     * Checks if the game is finished, and if not, loads the next level of the game.
     * @return true if there is a next level, false if not
     */
    public boolean nextLevel() {
        levelNumber++;

        //check if the game is finished
        if (levelNumber == allLevelsInfo.size()) {
            model.setState(GameState.FINISHED);
            model.setMenu();
            return false;
        }

        loadLevel();
        return true;
    }

    /** 
     * Creates a new {@link Level} based on the information from the current spot in the level information list. 
     * Then, the method updates the current level to the loaded level and feeds the level into the game.
     */
    private void loadLevel() {
        //create new level
        Level level = new Level(
            allLevelsInfo.get(levelNumber).category,
            allLevelsInfo.get(levelNumber).number,
            allLevelsInfo.get(levelNumber).platforms,
            allLevelsInfo.get(levelNumber).ladders,
            allLevelsInfo.get(levelNumber).monsters,
            allLevelsInfo.get(levelNumber).door
        );

        //sets the current level to the new level
        currentLevel = level;

        //Feed information into the game
        model.setPlatforms(level.getPlatforms());
        model.setLadders(level.getLadders());
        model.setMonsters(level.getMonsters());
        model.setDoor(level.getDoor());
        model.setLevelNumber(level.getLevelNumber());
        model.setLevelCategory(level.getLevelCategory());
    }


    /** @return the current {@link Level} of the manager. */
    public ILevel getCurrentLevel() {
        return currentLevel;
    }

    /** @return the list of level information of the manager. */
    public ArrayList<LevelStorer> getAllLevelsInfo() {
        return allLevelsInfo;
    }
}
