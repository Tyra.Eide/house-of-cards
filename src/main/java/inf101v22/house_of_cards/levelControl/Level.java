package inf101v22.house_of_cards.levelControl;

import java.awt.Dimension;
import java.util.ArrayList;

import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.screenObjects.levelDesign.Door;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.ClubGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.DiamondGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.HeartGuard;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.guards.SpadeGuard;

/**
 * Level container for House of Cards.
 * Stores initial {@link Platform}s, {@link Ladder}s, {@link Monster}s and {@link Door},
 * as well as the {@link LevelCategory} and level number within the category.
 * Extends {@link ILevel}.
 * @author Tyra Fosheim Eide
 */
public class Level implements ILevel{
    private LevelCategory category;
    private int number;

    private ArrayList<Platform> platforms;
    private ArrayList<Ladder> ladders;
    private ArrayList<Monster> monsters;
    private Door door;


    /**
     * Creates a new {@code Level} from arrays of ints specifying what level objects are in the level.
     * @param category {@link LevelCategory} of the level
     * @param number the number within the level category of the level
     * @param platforms two dimentional array of ints containing x-coordinates, y-coordinates, width and height of all the level's {@link Platform}s
     * @param ladders two dimentional array of ints containing x-coordinates, y-coordinates, width and height of all the level's {@link Ladder}s
     * @param monsters two dimentional array of ints containing x-coordinates, y-coordinates, direction and range of all the level's {@link Monster}s
     * @param door array of ints specifying the x-coordinate, y-coordinate, width and height if the level's {@link Door}
     */
    public Level(LevelCategory category, int number, int[][] platforms, int[][] ladders, int[][] monsters, int[] door) {
        this.category = category;
        this.number = number;

        initializePlatforms(platforms);
        initializeLadders(ladders);
        initializeMonsters(monsters);
        initializeDoor(door);
        initializeWalls();
    }


    @Override
    public LevelCategory getLevelCategory() {
        return category;
    }

    @Override
    public int getLevelNumber() {
        return number;
    }

    
    /**
     * Takes a two dimentional array specifying x-positions, y-positions, widths and heights
     * of all platforms and makes {@link Platform} objects, storing them in the {@code Level}.
     * @param platforms the array specifying x, y, width and height of all platforms.
     */
    private void initializePlatforms(int[][] platforms) {
        this.platforms = new ArrayList<>();

        for (int platform = 0; platform < platforms[0].length; platform++) {
            Platform newPlatform = new Platform(platforms[0][platform] + HOCModel.getOffset(), platforms[1][platform], new Dimension(platforms[2][platform], platforms[3][platform]));
            this.platforms.add(newPlatform);
        }
    }

    /**
     * Takes a two dimentional array specifying x-positions, y-positions, widths and heights
     * of all ladders and makes {@link Ladder} objects, storing them in the {@code Level}.
     * @param ladder the array specifying x, y, width and height of all ladders.
     */    
    private void initializeLadders(int[][] ladders) {
        this.ladders = new ArrayList<>();

        for (int ladder = 0; ladder < ladders[0].length; ladder++) {
            Ladder newLadder = new Ladder(ladders[0][ladder] + HOCModel.getOffset(), ladders[1][ladder], new Dimension(ladders[2][ladder], ladders[3][ladder]));
            this.ladders.add(newLadder);
        }
    }


    /**
     * Takes a two dimentional array specifying x-positions, y-positions, directions and ranges
     * of all monsters and makes {@link Monster} objects of the type specified by the level name, 
     * storing them in the {@code Level}.
     * @param monsters the array specifying x, y, direction and range of all monsters.
     */    
    private void initializeMonsters(int[][] monsters) {
        this.monsters = new ArrayList<>();

        for (int monster = 0; monster < monsters[0].length; monster++) {
            if (category == LevelCategory.CLUBS) {
                Monster newMonster = new ClubGuard(monsters[0][monster] + HOCModel.getOffset(), monsters[1][monster], monsters[2][monster], monsters[3][monster]);
                this.monsters.add(newMonster);
            }

            else if (category == LevelCategory.DIAMONDS) {
                Monster newMonster = new DiamondGuard(monsters[0][monster] + HOCModel.getOffset(), monsters[1][monster], monsters[2][monster], monsters[3][monster]);
                this.monsters.add(newMonster);
            }

            else if (category == LevelCategory.HEARTS) {
                Monster newMonster = new HeartGuard(monsters[0][monster] + HOCModel.getOffset(), monsters[1][monster], monsters[2][monster], monsters[3][monster]);
                this.monsters.add(newMonster);
            }

            else if (category == LevelCategory.SPADES) {
                Monster newMonster = new SpadeGuard(monsters[0][monster] + HOCModel.getOffset(), monsters[1][monster], monsters[2][monster], monsters[3][monster]);
                this.monsters.add(newMonster);
            }
        }
    }

    /**
     * Takes an array of ints specifying the x, y, width and height of to door
     * in the level, and make the {@link Door}, storing it in the level.
     * @param door the array specifying x, y, width and height of the door
     */
    private void initializeDoor(int[] door) {
        if (door != null) {
            this.door = new Door(door[0] + HOCModel.getOffset(), door[1], new Dimension(door[2], door[3]));
        }
    }

    /**
     * Sets the walls of the level based on the preferred size of the game and adds
     * them to the {@code Level} as {@link Platform}s.
     */    
    private void initializeWalls() {
        Platform wallTop = new Platform(HOCModel.getOffset(), 0, new Dimension(HOCModel.getPreferredSize().width, 2));
        Platform wallRight = new Platform(HOCModel.getPreferredSize().width + HOCModel.getOffset() - 2, 0, new Dimension(2, HOCModel.getPreferredSize().height));
        Platform wallBottom = new Platform(HOCModel.getOffset(), HOCModel.getPreferredSize().height - 2, new Dimension(HOCModel.getPreferredSize().width, 2));
        Platform wallLeft = new Platform(HOCModel.getOffset(), 0, new Dimension(2, HOCModel.getPreferredSize().height));

        Platform[] walls = new Platform[]{wallBottom, wallLeft, wallRight, wallTop};

        for (Platform wall : walls) {
            platforms.add(wall);
        }
    }

    @Override
    public ArrayList<Platform> getPlatforms() {
        return platforms;
    }

    @Override
    public ArrayList<Ladder> getLadders() {
        return ladders;
    }

    @Override
    public ArrayList<Monster> getMonsters() {
        return monsters;
    }


    @Override
    public Door getDoor() {
        return door;
    }
    
}
