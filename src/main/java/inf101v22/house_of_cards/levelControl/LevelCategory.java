package inf101v22.house_of_cards.levelControl;

/**
 * The different categories of levels in House of Cards.
 * There is one category for each suit in a deck of cards.
 * @author Tyra Fosheim Eide
 */
public enum LevelCategory {
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES;

    public static int getNumber(LevelCategory category) {
        if (category == CLUBS) {
            return 0;
        }
        
        else if (category == DIAMONDS) {
            return 1;
        }

        else if (category == HEARTS) {
            return 2;
        }

        else if (category == SPADES) {
            return 3;
        }

        return 4;
    }
}
