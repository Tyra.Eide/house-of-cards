package inf101v22.house_of_cards.levelControl;

import java.util.ArrayList;

import inf101v22.house_of_cards.screenObjects.levelDesign.Door;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;


/** 
 * Methods neccesary to access level information.
 * That involves getting the number and {@link LevelCategory} of the level as
 * well as all uncontrollable {@link ScreenObject}s of the level.
 * @author Tyra Fosheim Eide
 */
public interface ILevel {

    /** @return the category of the level. */
    public LevelCategory getLevelCategory();

    /** @return  the number of the level within its category. */
    public int getLevelNumber();
    
    /** @return platforms of the level. */
    public ArrayList<Platform> getPlatforms();

    /** @return ladders of the level. */
    public ArrayList<Ladder> getLadders();

    /** @return monsters of the level. */
    public ArrayList<Monster> getMonsters();

    /** @return the door of the level. */
    public Door getDoor();

}
