package inf101v22.house_of_cards.menuSystem;

import java.awt.Color;
import java.awt.Graphics;

import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.model.HOCModel;

/**
 * A MenuManager for Hosue of Cards.
 * Keeps track of all the possible menus and makes sure that the correct model is displayed
 * based on the state of the game.
 * @author Tyra Fosheim Eide
 */
public class MenuManager {
    private HOCModel model;
    private Menu menu;

    private final String[] mainMenuButtons = {"START GAME", "EXIT"};
    private final GameState[] mainMenuStates = {GameState.START, GameState.CLOSE};

    private final String[] pauseMenuButtons = {"CONTINUE", "EXIT TO MAIN MENU", "EXIT TO DESKTOP"};
    private final GameState[] pauseMenuStates = {GameState.ACTIVE, GameState.WELCOME, GameState.CLOSE};

    private final String[] gameFinishedMenuButtons = {"REPLAY", "MAIN MENU"};
    private final GameState[] gameFinishedMenuStates = {GameState.START, GameState.WELCOME};


    /**
     * Creates a new MenuManager for a {@link HOCModel}.
     * @param model the {@link HOCModel} to manage the menus of
     */
    public MenuManager(HOCModel model) {
        this.model = model;
        setMenu();

    }

    /** Sets the current menu based on the {@link GameState} of the game */
    public void setMenu() {
        if (model.getState() == GameState.WELCOME) {
            menu = new Menu(HOCModel.getPreferredSize(), mainMenuButtons, mainMenuStates, "House of Cards");
        }

        else if (model.getState() == GameState.PAUSED) {
            menu = new Menu(HOCModel.getPreferredSize(), pauseMenuButtons, pauseMenuStates, "Paused");
        }

        else if (model.getState() == GameState.GAME_OVER) {
            menu = new Menu(HOCModel.getPreferredSize(), gameFinishedMenuButtons, gameFinishedMenuStates, "GAME OVER");
        }

        else if (model.getState() == GameState.FINISHED) {
            menu = new Menu(HOCModel.getPreferredSize(), gameFinishedMenuButtons, gameFinishedMenuStates, "TO BE CONTINUED");
        }
    }

    /** @return the current {@link Menu} */
    public Menu getMenu() {
            return menu;
    }

    /** 
     * Draws the current {@link Menu}. 
     * @param g the component on which to draw the {@link Menu}
     * @param titleColor the color in which to draw the title of the {@link Menu}
     */
    public void drawMenu(Graphics g, Color titleColor) {
        if (menu != null) {
            menu.drawMenu(g, titleColor);
        }
    }
}
