package inf101v22.house_of_cards.menuSystem;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.view.GraphicHelperMethods;

/**
 * A button that can change the {@link GameState} of the game.
 * Extends {@link ScreenObject}.
 * @author Tyra Fosheim Eide
 */
public class Button extends ScreenObject {
    private String name;

    private boolean mouseOver;
    private boolean mouseClicked; // for possible where buttons have costumes

    private GameState state;

    /**
     * Creates a new button at a specified location, with the specified {@link GameState}.
     * @param position the x- and y- coordinates of the button
     * @param dimension the width and height of the button
     * @param name the text to display on the button
     * @param state the state to change the game into when the button is pressed
     */
    public Button(Point position, Dimension dimension, String name, GameState state) {
        super(position.x, position.y, dimension);

        this.name = name;
        this.state = state;
    }

    /** @return whether or not the Button is being hovered over by the mouse. */
    public boolean isMouseOver() {
        return mouseOver;
    }

    /** Tells the button that it is being hovered over by the mouse. */
    public void setMouseOver(boolean mouseOver) {
        this.mouseOver = mouseOver;
    }

    /** @return whether or not the Button is being clicked by the mouse. */
    public boolean isMouseClicked() {
        return mouseClicked;
    }

    /** Tells the button that it is being clicked by the mouse. */
    public void setMouseClicked(boolean mouseClicked) {
        this.mouseClicked = mouseClicked;
    }

    /**
     * Draws the {@code Button} with the button text in a given color.
     * @param g the canvas to draw the button on
     */
    public void drawButton(Graphics g) {

        if (mouseOver) {
            g.setColor(new Color(37, 112, 34));
        }

        else if (!mouseOver) {
            g.setColor(Color.GREEN);
        }
        
        g.fillRect(x, y, dimension.width, dimension.height);

        g.setColor(Color.BLACK);
        g.drawRect(x, y, dimension.width, dimension.height);

        Font font = new Font("SansSerif", Font.BOLD, 25);
        g.setFont(font);

        GraphicHelperMethods.drawCenteredString(g, name, x, y, dimension.width, dimension.height);
    }

    /** @return the {@link GameState} the button changes the game to when clicked. */
    public GameState getState() {
        return state;
    }
    
}
