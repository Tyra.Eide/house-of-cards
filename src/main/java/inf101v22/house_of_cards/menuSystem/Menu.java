package inf101v22.house_of_cards.menuSystem;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.awt.Font;

import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.view.GraphicHelperMethods;

/**
 * A menu of House of Card. Acts as a container for a set of {@link Button}s.
 * @author Tyra Fosheim Eide
 */
public class Menu {

    private int x;
    private int y;
    private int width;
    private int height;

    private ArrayList<Button> buttons;

    private String title;

    /**
     * Creates a new set of buttons to fill a Menu of a given dimension, based on a list of button names and states. Titles the menu accordingly.
     * @param dimension the width and height of the Menu
     * @param buttonNames an array of all button names of the menu
     * @param buttonStates an array of all button states of the menu
     * @param title the title of the menu
     */
    public Menu(Dimension dimension, String[] buttonNames, GameState[] buttonStates, String title) {

        this.title = title;

        width = dimension.width / 3;
        height = dimension.height / 5;

        x = dimension.width /2 - width/2;
        y = dimension.height /2 - height/2;

        initializeButtons(buttonNames, buttonStates);

    }

    /**
     * Pairs information from an array of button names and an array of button states and creates a list of buttons, assigning it to the Menu.
     * @param buttonNames the array of button names
     * @param buttonStates the array of button states
     */
    private void initializeButtons(String[] buttonNames, GameState[] buttonStates) {
        buttons = new ArrayList<>();

        for (int index = 0; index < buttonNames.length; index++) {
            int yPos = y + height/buttonNames.length * index;
            int buttonHeight = height/buttonNames.length;
            Button button = new Button(new Point(x + HOCModel.getOffset(), yPos), new Dimension(width, buttonHeight), buttonNames[index], buttonStates[index]);
            
            buttons.add(button);
        }
    }

    /**
     * Draws the title of the Menu
     * @param g the component on which to draw the title
     * @param titleColor the color to draw the title in
     */
    private void drawTitle(Graphics g, Color titleColor) {
        g.setColor(titleColor);

        Font font = new Font("SansSerif", Font.BOLD, 30);
        g.setFont(font);
        GraphicHelperMethods.drawCenteredString(g, title, x + HOCModel.getOffset(), y - 75, width, height/5);
    }

    /**
     * Draws all buttons of the menu
     * @param g the component on which to draw the buttons
     */
    private void drawButtons(Graphics g) {
        for (Button button : buttons) {
            button.drawButton(g);
        }

    }
    
    /**
     * Draws the menu buttons and title
     * @param g the component on which to draw the menu
     * @param titleColor the color to draw the menu title in
     */
    protected void drawMenu(Graphics g, Color titleColor) {
        drawTitle(g, titleColor);

        drawButtons(g);
    }

    /** @return all the Menu's {@link Button}s */
    public ArrayList<Button> getButtons() {
        return buttons;
    }

    /** Sets all the Menu's {@link Button}s to being not hovered over and not clicked. */
    public void resetButtons() {
        for (Button button : buttons) {
            button.setMouseClicked(false);
            button.setMouseOver(false);
        }
    }

}
