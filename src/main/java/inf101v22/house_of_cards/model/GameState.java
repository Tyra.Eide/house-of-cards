package inf101v22.house_of_cards.model;

/** 
 * The possible states of a game of House of Cards.
 * @author Tyra Fosheim Eide  
 */
public enum GameState {
    ACTIVE,
    PAUSED,
    WELCOME,
    GAME_OVER,
    CLOSE,
    FINISHED,
    START,
    NEXT_LEVEL
}
