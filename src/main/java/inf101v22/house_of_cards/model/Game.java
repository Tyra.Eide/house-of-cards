package inf101v22.house_of_cards.model;

import java.util.ArrayList;
import inf101v22.house_of_cards.levelControl.LevelCategory;
import inf101v22.house_of_cards.screenObjects.levelDesign.Door;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectile;

/**
 * Storing the non-playable {@link ScreenObject}s on the game board.
 * This includes platforms, ladders, monsters, projectiles and doors as well
 * as level number and category.
 * @author Tyra Fosheim Eide
 */
public class Game {
    protected ArrayList<Platform> platforms;
    protected ArrayList<Ladder> ladders;
    protected ArrayList<Monster> monsters;
    protected ArrayList<Projectile> projectiles;
    protected Door door;

    protected int levelNumber;
    protected LevelCategory levelCategory;
    
    public Game() {
        projectiles = new ArrayList<>();
    }

}
