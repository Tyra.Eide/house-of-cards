package inf101v22.house_of_cards.model;

import java.util.ArrayList;

import inf101v22.house_of_cards.screenObjects.ScreenObject;

/**
 * Helper methods for the class HOCModel.
 * @author Tyra Fosheim Eide
 */
public class HOCModelHelperMethods {
    
    /**
     * Casts a list of objects who are inheriters of ScreenObject to a list of ScreenObjects.
     * @param <E> the type to cast
     * @param list the list to cast
     * @return the list of ScreenObjects
     * @see <a href = https://stackoverflow.com/questions/33807723/casting-arraylist> Source </a>
     */
    public static <E> ArrayList<ScreenObject> castArrayList(ArrayList<E> list) {   
        ArrayList<ScreenObject> newlyCastedArrayList = new ArrayList<>();
        
        if (list.get(0).getClass().isAssignableFrom(ScreenObject.class)) {
            throw new ClassCastException("Couldn't cast class" + list.getClass() + "to ScreenObject.");
        }

        for (E listObject : list) {
            newlyCastedArrayList.add((ScreenObject)listObject);
        }

        return newlyCastedArrayList;
    }
}
