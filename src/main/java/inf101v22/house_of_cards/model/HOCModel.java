package inf101v22.house_of_cards.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.ArrayList;

import inf101v22.house_of_cards.controller.HOCControllable;
import inf101v22.house_of_cards.levelControl.LevelCategory;
import inf101v22.house_of_cards.levelControl.LevelManager;
import inf101v22.house_of_cards.menuSystem.Menu;
import inf101v22.house_of_cards.menuSystem.MenuManager;
import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.screenObjects.levelDesign.Door;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.Sprite;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.player.Player;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectile;
import inf101v22.house_of_cards.view.HOCViewable;

public class HOCModel implements HOCViewable, HOCControllable{
    private Player player;

    private GameState state;
    
    private Game game;

    private MenuManager menuManager;
    private LevelManager levelManager;

    private int playerAttackCoolDown;

    /** Creates an empty model and lets the LevelManager fill in level information. */
    public HOCModel() {
        game = new Game();

        initializeModel();
    }

    /**
     * Creates a model with a given player and given lists of {@link Platform}s, 
     * {@link Ladder}s and {@link Monster}s.
     * @param player
     * @param platforms list of all platform objects
     * @param ladders list of all ladder objects
     * @param monsters list of all monster objects
     */
    public HOCModel(Player player, ArrayList<Platform> platforms, ArrayList<Ladder> ladders, ArrayList<Monster> monsters) {
        this.player = player;
        
        game = new Game();
        game.platforms = platforms;
        game.ladders = ladders;
        game.monsters = monsters;

        initializeModel();
    }

    /** Sets non-input field variables for the model. */
    private void initializeModel() {
        levelManager = new LevelManager(this);
        this.state = GameState.WELCOME;
    
        menuManager = new MenuManager(this);
        setMenu();

        playerAttackCoolDown = 0;
    }

    @Override
    public GameState getState() {
        return state;
    }

    @Override
    public void setState(GameState state) {
        this.state = state;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    /**
     * Sets the model's {@link Player}.
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public ArrayList<Platform> getPlatforms() {
        return game.platforms;
    }

    /**
     * Sets the model's current {@link Platform}s
     * @param platforms
     */
    public void setPlatforms(ArrayList<Platform> platforms) {
        game.platforms = platforms;
    }

    @Override
    public ArrayList<Ladder> getLadders() {
        return game.ladders;
    }

    /**
     * Sets the model's current {@link Ladder}s
     * @param ladders
     */
    public void setLadders(ArrayList<Ladder> ladders) {
        game.ladders = ladders;
    }

    @Override
    public ArrayList<Projectile> getProjectiles() {
        return game.projectiles;
    }

    /**
     * Sets the model's current {@link Projectile}s
     * @param projectiles
     */
    public void setProjectiles(ArrayList<Projectile> projectiles) {
        game.projectiles = projectiles;
    }

    @Override
    public ArrayList<Monster> getMonsters() {
        return game.monsters;
    }

    /**
     * Sets the model's current {@link Monster}s
     * @param monsters
     */
    public void setMonsters(ArrayList<Monster> monsters) {
        game.monsters = monsters;
    }

    @Override
    public Door getDoor() {
        return game.door;
    }

    /**
     * Sets the model's current {@link Door}
     * @param door
     */
    public void setDoor(Door door) {
        game.door = door;
    }

    @Override
    public int getLevelNumber() {
        return game.levelNumber;
    }

    /**
     * Sets the model's current level number
     * @param levelNumber
     */
    public void setLevelNumber(int levelNumber) {
        game.levelNumber = levelNumber;
    }

    @Override
    public LevelCategory getLevelCategory() {
        return game.levelCategory;
    }

    /**
     * Sets the model's current {@link LevelCategory}
     * @param levelCategory
     */
    public void setLevelCategory(LevelCategory levelCategory) {
        game.levelCategory = levelCategory;
    }
    
    /**
     * Checks if a {@code Sprite} collides with any {@link ScreenObject} from a
     * given list, and calls the appropriate reaction method for the {@code Sprite}.
     * @param actor the Sprite to check for collision
     * @param colliders the list of potential colliders
     */
    private void checkCollision(Sprite actor, ArrayList<ScreenObject> colliders) {
        
        //VERTICAL COLLISION CHECK
        //check for collision for each ScreenObject
        for (ScreenObject collider : colliders) {
            actor.checkVerticalCollision(collider);
        }

        //HORISONTAL COLLISION CHECK
        //check for collision for each ScreenObject
        for (ScreenObject collider : colliders) {
            actor.checkHorizontalCollision(collider);
        }

    }

    /** Checks if the player of the model is standing on a platform. */
    private boolean playerCheckStandingOnPlatform() {

        if (game.platforms != null) {
            player.moveHitbox(0, 1);
            
            for (Platform platform : game.platforms) {
                if (player.midpointBotomIsColliding(platform)) {
                    player.setStandingOnPlatform(true);
                    break;
                }
                player.setStandingOnPlatform(false);
            }
            player.moveHitbox(0, -1);
        }

        return player.isStandingOnPlatform();
    }

    /**
     * Determines whether or not the {@link Player} can pass {@code through platforms}
     * on their way off or on a given {@link Ladder}.
     * If the {@link Player} is above the middle of the {@link Ladder}, they can step
     * onto or off the ladder through the {@link Platform} the {@link Ladder} is linked to.
     * @param ladder the ladder to calculate intangibility in relation to
     */
    private void playerIsIntangible(Ladder ladder) {
        int middleOfladder = ladder.getY() + ladder.getDimension().height/2;                

        if (player.getMidpointBotom().y <= middleOfladder) {
            player.setIntangible(true);
        }

        else {
            player.setIntangible(false);
        }
    }

    /**
     * Checks if the {@link Player} has the potential of climbing in a {@link Ladder} of the model.
     * The method also updates the player's intagibility based on its placement in relation to the ladder.
     * @param down whether to check if player can climb down. This requires to check if there is a {@link Ladder} direcetly underneath the {@link Player} as well.
     * @return true if the player can climb the ladder, false if not.
     */

    private boolean playerCanClimbLadder(boolean down) {

        boolean playerCanClimbLadder = false;

        if (game.ladders != null) {
            if (down) player.moveHitbox(0, 1);
            for (Ladder ladder : game.ladders) {
                
                //Allow the climb if the player is in contact with a ladder.
                if (player.isColliding(ladder)) {
                    playerCanClimbLadder = true;

                    //Check if the player can climb through platforms as well
                    playerIsIntangible(ladder);
                    break;
                }
            }
            if (down) player.moveHitbox(0, -1);
        }
        return playerCanClimbLadder;
    }

    /** Cool down for player attack. Takes 20 frames to cool fully. */
    private void playerAttackCoolDown() {
        if (playerAttackCoolDown > 0) {
            playerAttackCoolDown--;
        }
    }

    /** 
     * Cheks if the player has the potential to attack and resets the cool down when attack is ordered.
     * @param resetCoolDown whether or not to reset the cool down 
     */
    private boolean playerCanAttack(boolean resetCoolDown) {
        //Cool down attack.
        playerAttackCoolDown();

        boolean playerCanAttack = false;

        //Let the attack through if cool down is finished
        if (playerAttackCoolDown <= 0) {
            playerCanAttack = true;

            //reset cooldown only if instructed
            if (resetCoolDown) playerAttackCoolDown = 20;
        }

        return playerCanAttack;
    }


    /** Spawns projectiles in the type of the player's weapon. */
    private void playerAttack() {
        if (player.getDirection() == -1) {
            Projectile weapon = new Projectile(player.getX() - 26, player.getY() + player.getDimension().height/2, player.getWeapon(), -1);
            game.projectiles.add(weapon);
        }

        else if (player.getDirection() == 1) {
            Projectile weapon = new Projectile(player.getX() + player.getDimension().width, player.getY() + player.getDimension().height/2, player.getWeapon(), 1);
            game.projectiles.add(weapon);
        }

    }

    /**
     * Update the position and state of the {@link Player} in the model.
     * @param left instruction to move left
     * @param right instruction to move right
     * @param jump instruction to jump
     * @param attack instruction to attack
     */
    private void updatePlayer(boolean left, boolean right, boolean up, boolean down, boolean jump, boolean attack) {
        
        //Update player lives
        if (!player.getLives().isEmpty()) player.updateLives();

        //Disable jump and attack command if player is not standing on platform      
        playerCheckStandingOnPlatform();

        //Disable movement up or down if the player cannot climb
        if (!playerCanClimbLadder(down)) {
            up = false;
            down = false;
        }

        //Disable attack if cool down has not been reached
        if (!playerCanAttack(attack)) {
            attack = false;
        }

        //Plan the move that is about to be executed and set the costume
        player.planMove(left, right, up, down, jump, attack);

        if (player.isDead()) {
            state = GameState.GAME_OVER;
            setMenu();
        }

        else {

            //Check for collision and affect the planned move appropriately
            if (game.platforms != null) {
                if (!game.platforms.isEmpty()) {
                    checkCollision(player, HOCModelHelperMethods.castArrayList(game.platforms)); 
                }
            } 
            
            if (game.ladders != null) {
                if (!game.ladders.isEmpty()) {
                    checkCollision(player, HOCModelHelperMethods.castArrayList(game.ladders));
                }
            }

            //Move the planned amount
            player.movePosition(player.getSpeedx(), player.getSpeedy());
            player.setHitbox();

            //Initiate attack if ordered
            if (attack && player.isAttacking()) {
                playerAttack();
            }
        }
    }


    /** Updates the position of all the current {@link Monster}s of the model */
    private void updateMonsters() {
        //A storing list for all dead monsters, they will be removed from the model
        ArrayList<Monster> finishedMonsters = new ArrayList<>();

        for (Monster monster : game.monsters) {

            if (monster.isDead()) {
                finishedMonsters.add(monster);
                continue;
            }

            monster.planMove();
            if (!game.platforms.isEmpty()) checkCollision(monster, HOCModelHelperMethods.castArrayList(game.platforms));
            if (!player.isDying()) monster.checkHit(player);

            monster.movePosition(monster.getSpeedx(), monster.getSpeedy());
            monster.setHitbox();
        }
        //remove finished monsters
        game.monsters.removeAll(finishedMonsters);
    }

    /** Update position and costume of all the current {@link Projectile}s in the model. */
    private void updateProjectiles() {
        
        //a storing list for all projectiles that are finished, and are to be removed from the model
        ArrayList<Projectile> finishedProjectiles = new ArrayList<>();

        for (Projectile projectile : game.projectiles) {
            
            if (!projectile.isVisible()) {
                finishedProjectiles.add(projectile);
                continue;
            }
            
            projectile.planMove();
            if (game.platforms != null) checkCollision(projectile, HOCModelHelperMethods.castArrayList(game.platforms));
            if (game.monsters != null) {
                for (Monster monster : game.monsters) {
                    projectile.checkHit(monster);
                }
            }
            
            projectile.movePosition(projectile.getSpeedx(), projectile.getSpeedy());
            projectile.moveHitbox(projectile.getSpeedx(), projectile.getSpeedy());
            
        }
        //remove finished projectiles
        game.projectiles.removeAll(finishedProjectiles);
    }

    /** 
     * Prosedure for going to the next level of the game.
     * Moves the player to the starting position if the next level is loaded, and sets the GameState to NEXT_LEVEL. 
     */
    private void nextLevel() {
        if (levelManager.nextLevel()) {
            player.setPosition(4 + getOffset(), getPreferredSize().height - 66);
            player.setSpeed(0, 0);
            state = GameState.NEXT_LEVEL;
        }
    }

    /** 
     * Updates the current door of the model by checking if the player is going through it. 
     * Tells the model to go to the next level when the player goes through.
     */
    private void updateDoor() {
        if (game.door != null) {
            //check if player has entered door and go to next level if so

            //move the player's hitbox up one pixel to get into the door if it is there
            player.moveHitbox(0, -1);
            player.setMidpointBotom();

            if (player.midpointBotomIsColliding(game.door)) {
                nextLevel();
            }

            player.moveHitbox(0, 1);
            player.setMidpointBotom();
        }
    }

    /**
     * Update the player, projectiles and monsters of the model by
     * by moving them all and changing their costumes respective to their states.
     * @param left instruction for {@link Player} movement
     * @param right instruction for {@link Player} movement     
     * @param up instruction for {@link Player} movement
     * @param down instruction for {@link Player} movement
     * @param jump instruction for {@link Player} movement

     * @param attack instruction for {@link Player} movement
     */
    @Override
    public void updateModel(boolean left, boolean right, boolean up, boolean down, boolean jump, boolean attack) {
        updatePlayer(left, right, up, down, jump, attack);
        
        if (game.projectiles != null) updateProjectiles();
        if (game.monsters != null) updateMonsters();
        updateDoor();
    }

    @Override
    public void setMenu() {
        menuManager.setMenu();
    }

    @Override
    public Menu getMenu() {
        return menuManager.getMenu();
    }

    @Override
    public void drawMenu(Graphics g, Color titleColor) {
        menuManager.drawMenu(g, titleColor);
    }

    /** @return the size of the game. */
    public static Dimension getPreferredSize() {
        return new Dimension(800, 864);
    }

    /** @return where the game panel lies in the frame on the x-axis. */
    public static int getOffset() {
        return (Toolkit.getDefaultToolkit().getScreenSize().width - getPreferredSize().width) / 2;
    }

    /** 
     * @return where the item bar of the game is in the frame and its dimension. 
     * This imnformation comes in the form of an array of ints, where the first int is
     * x, the second is y, the third is width and the fourth is height.
     * */
    public static int[] getItemBarBounds() {
        int x = getOffset();
        int y = 0;
        int width = getPreferredSize().width;
        int height = 40;

        int[] itemBarBounds = {x, y, width, height};

        return itemBarBounds;
    }

    @Override
    public void startGame() {
        player = new Player(4 + getOffset(), getPreferredSize().height - 66);
        levelManager.startGame();
    }

}
