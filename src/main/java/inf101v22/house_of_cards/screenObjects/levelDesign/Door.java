package inf101v22.house_of_cards.screenObjects.levelDesign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import inf101v22.house_of_cards.screenObjects.ScreenObject;

/**
 * A door of House of Cards.
 * This can be used to get to the next level of the game.
 * Extends {@link ScreenObject}.
 * @author Tyra Fosheim Eide
 */
public class Door extends ScreenObject{
    private boolean active;

    /**
     * Creates a new door with the given position and dimension
     * @param x x-coordinate
     * @param y y-coordinate
     * @param dimension width and height
     */
    public Door(int x, int y, Dimension dimension) {
        super(x, y, dimension);
        active = false;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
    

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(x, y, dimension.width, dimension.height);
    }
}
