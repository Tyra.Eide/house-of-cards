package inf101v22.house_of_cards.screenObjects.levelDesign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import inf101v22.house_of_cards.screenObjects.ScreenObject;

/**
 * A platform of House of Cards.
 * Extends {@link ScreenObject}.
 * @author Tyra Fosheim Eide
 */
public class Platform extends ScreenObject{
    
    /**
    * Creates a new platform with the given position and dimension
    * @param x x-coordinate
    * @param y y-coordinate
    * @param dimension width and height
    */
    public Platform(int x, int y, Dimension dimension) {
        super(x, y, dimension);
    }

    /**
     * Draws a {@code Platform} in the view.
     * @param g the panel to draw on
     */
    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(x, y, dimension.width, dimension.height);
    }
}
