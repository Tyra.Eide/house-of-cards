package inf101v22.house_of_cards.screenObjects.levelDesign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import inf101v22.house_of_cards.screenObjects.ScreenObject;

/**
 * A Ladder of House of Cards.
 * Extends {@link ScreenObject}.
 * @author Tyra Fosheim Eide
 */
public class Ladder extends ScreenObject{

    private final Color brown = new Color(153, 76, 0);
    private final int poleWidth = 5;
    
    /**
     * Creates a new ladder with the given position and dimension
     * @param x x-coordinate
     * @param y y-coordinate
     * @param dimension width and height
     */
    public Ladder(int x, int y, Dimension dimension) {
        super(x, y, dimension);
        setHitbox(new Rectangle(x + (dimension.width / 2), y, 2, dimension.height));
    }

    /**
     * Draws the poles of the ladder
     * @param g the component on which to draw
     */
    private void drawPoles(Graphics g) {
        //Filling
        g.setColor(brown);
        g.fillRect(x, y, poleWidth, dimension.height);
        g.fillRect(x + dimension.width - poleWidth, y, poleWidth, dimension.height);

        //Outline
        g.setColor(Color.BLACK);
        g.drawRect(x, y, poleWidth, dimension.height);
        g.drawRect(x + dimension.width - poleWidth, y, poleWidth, dimension.height);

    }

    /**
     * Draws the bars of the ladder
     * @param g the component on which to draw
     */
    private void drawBars(Graphics g) {
        //go down the length of the ladder, draw a bar every ten pixels
        for (int counter = 1; counter < dimension.height; counter++) {
           
            if (counter % 20 == 0 && (counter + poleWidth) < dimension.height) {
                //filling
                g.setColor(brown);
                g.fillRect(x + poleWidth, y + counter, dimension.width - poleWidth* 2, poleWidth);

                g.setColor(Color.BLACK);
                g.drawRect(x + poleWidth, y + counter, dimension.width - poleWidth* 2, poleWidth);
            
            }
        }
    }

    /**
     * Draws a {@code Ladder} in the view.
     * @param g the panel to draw on
     */
    public void draw(Graphics g) {
        //drawHitbox(g);
        
        drawPoles(g);
        drawBars(g);
    }
    

}
