package inf101v22.house_of_cards.screenObjects.sprites.player;

/**
 * Possible player states in house of cards.
 * @author Tyra Fosheim Eide
 */
public enum State {
    STANDING,
    RUNNING,
    CHARGING,
    JUMPING,
    FALLING,
    CLIMBING,
    ATTACKING,
    DAMAGED,
    DEAD
}
