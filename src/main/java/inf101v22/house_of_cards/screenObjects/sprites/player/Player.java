package inf101v22.house_of_cards.screenObjects.sprites.player;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.Damageable;
import inf101v22.house_of_cards.screenObjects.sprites.Sprite;
import inf101v22.house_of_cards.screenObjects.sprites.icons.LifeHeart;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.PaperCard;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectible;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectile;

/**
 * Player sprite for House of Cards.
 * Can run, climb, jump and attack, as well as take damage.
 * Extends {@link Sprite} and implements {@link Damageable}
 * @author Tyra Fosheim Eide
 */
public class Player extends Sprite implements Damageable {
    private static final String SPRITESHEET_PATH = "sprites/Player/Janus Girl Spritesheet.png";
    private static final int SPRITE_ROW = 5;
    private static final int SPRITE_COLUMN = 6;

    public static final int gravity = 1;

    private PlayerCostumeManager costumeManager;
    
    private boolean jumping = false;
    private boolean attacking = false;
    private boolean climbing = false;
    private boolean hit = false;
    private boolean dying = false;
    private boolean dead = false;

    private boolean standingOnPlatform = false;
    private boolean intangible = false;
    

    private State state = State.STANDING;
    private int direction = 1;

    private Point midpointBotom;

    private Projectible weapon = new PaperCard();

    private int maximumHealth = 300;
    private int currentHealth = maximumHealth;
    private ArrayList<LifeHeart> lives;


    /**
     * Creates a new {@code Player} at the given position.
     * @param position the position in the view
     */
    public Player(int x, int y) {
        super(x, y, SPRITE_ROW, SPRITE_COLUMN, SPRITESHEET_PATH);
        costumeManager = new PlayerCostumeManager(this);

        lives = new ArrayList<>();
        
        setHitbox();
        setLives();
    }

    /** @return direction of the player */
    public int getDirection() {
        return direction;
    }

    /**
     * Sets the direction of the player.
     * @param direction
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /** Creates {@link LifeHeart}s for the player to represent its health. One heart is 100 hp. */
    private void setLives() {
        if (!lives.isEmpty()) lives.removeAll(lives);

        for (int i = 0; i < currentHealth/100; i++) {
            lives.add(new LifeHeart(HOCModel.getItemBarBounds()[0] + 10 + 11 * i, 10));
        }
    }

    @Override
    protected void setCurrentCostume(BufferedImage currentCostume) {
        super.setCurrentCostume(currentCostume);
    }

    /** 
     * Sets the hitbox of the {@code Player} according to the direction and state,
     * and updates the midpoint of the bottom to be the midpoint of the bottom of the adjusted hitbox.
     */
    public void setHitbox() {
        if (state == State.STANDING){
            if (direction == -1) {
                hitbox = new Rectangle(x + 12, y + 12, dimension.width - 35, dimension.height - 12);
            }

            else if (direction == 1) {
                hitbox = new Rectangle(x + 23, y + 12, dimension.width - 35, dimension.height - 12);
            }
        }

        if (state == State.RUNNING) {
            if (direction == -1) {
                hitbox = new Rectangle(x + 12, y + 12, dimension.width - 35, dimension.height - 12);
            }

            else if (direction == 1) {   
                hitbox = new Rectangle(x + 23, y + 12, dimension.width - 35, dimension.height - 12);
            }
        }

        if (state == State.CHARGING) {
            if (direction == -1) {
                hitbox = new Rectangle(x + 12, y + 16, dimension.width - 35, dimension.height - 16);
            }

            else if (direction == 1) {
                hitbox = new Rectangle(x + 23, y + 16, dimension.width - 35, dimension.height - 16);
            }
        }

        if (state == State.JUMPING) {
            if (direction == -1) {
                hitbox = new Rectangle(x + 12, y + 17, dimension.width - 35, dimension.height - 17);
            }

            else if (direction == 1) {
                hitbox = new Rectangle(x + 23, y + 17, dimension.width - 35, dimension.height - 17);
            }
        }

        if (state == State.FALLING || state == State.DAMAGED) {
            if (direction == -1) {
                hitbox = new Rectangle(x + 12, y + 12, dimension.width - 35, dimension.height - 12);
            }

            else if (direction == 1) {
                hitbox = new Rectangle(x + 23, y + 12, dimension.width - 35, dimension.height - 12);
            }
        }

        if (state == State.CLIMBING) {
            hitbox = new Rectangle(x + 12, y + 12, dimension.width - 24, dimension.height - 12);
        }

        setMidpointBotom();
    }

    public void setMidpointBotom() {
        midpointBotom = new Point(hitbox.x + (hitbox.width / 2), hitbox.y + hitbox.height);
    }

    public Point getMidpointBotom() {
        return midpointBotom;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public boolean isJumping() {
        return jumping;
    }

    public boolean isAttacking() {
        return attacking;
    }

    public boolean isDead() {
        return dead;
    }

    public boolean isDying() {
        return dying;
    }

    public void setIntangible(boolean intangible) {
        this.intangible = intangible;
    }

    public boolean isIntangible() {
        return intangible;
    }

    public void setClimbing(boolean climbing) {
        this.climbing = climbing;
    }

    public boolean isClimbing() {
        return climbing;
    }

    public ArrayList<LifeHeart> getLives() {
        return lives;
    }

    /** Updates the life hearts of the player. Removes empty lives from the list of hearts. */
    public void updateLives() {
        lives.get(lives.size() - 1).updateLifeHeart();

        if (!lives.get(lives.size() - 1).isVisible()) {
            //store the empty heart temporarily
            LifeHeart emptyHeart = lives.get(lives.size() - 1);

            //remove the empty heart from the heart list
            lives.remove(lives.size() - 1);

            //transfer any excess damage to the next heart.
            if (!lives.isEmpty()) lives.get(lives.size() - 1).decreaseHealth(emptyHeart.getLeftOverDamage());
        }
    }

    @Override
    public int getHealth() {
        return currentHealth;
    }

    private void checkHealth() {
        if (currentHealth <= 0) {
            dying = true;
        }

    }

    @Override
    public void damaged(int damage, int colliderDirection) {
        //only take damage if player is not already in damage loop
        if (!hit) {
            climbing = false;
            jumping = false;

            //take damage
            currentHealth -= damage;

            //check if player died
            checkHealth();

            //decrease life heart accordingly
            lives.get(lives.size() - 1).decreaseHealth(damage);

            //calculate how to bounce away from the collider

            //bounce away when standing still
            if (speedx == 0 && speedy == 0) {
                setSpeed(10*colliderDirection, -10);
                direction = colliderDirection;
            }

            //bounce away when running
            else if (speedx != 0) {
                setSpeed(3*-this.direction, -10);
            }

            //bounce away when falling
            else if (speedx == 0 && speedy != 0) {
                setSpeed(0, -15);
            }

            //update hit
            hit = true;
        }
    }

    @Override
    public boolean isHit() {
        return hit;
    }

    @Override
    public ScreenObject getScreenObject() {
        return this;
    }

    /** 
     * Sets the speed to max speed if the {@code Player} has exceeded 
     * the maximum speed horisontally or vertically.
     */
    private void checkMaxSpeed() {
        if (speedx > 5) speedx = 5;
        else if (speedx < -5) speedx = -5;

        if (speedy > 35) speedy = 35;
    }

    /**
     * Plans the next move of the {@code Player} according to the instructions given
     * by updating the speed and setting the movement state and direction of the {@code Player}.
     * @param left whether or not to move left
     * @param right whether or not to move right
     * @param up whether or not to move up
     * @param down whether or not to move down
     * @param jump whether or not to jump
     * @param attack whether or not to use attack
     */
    public void planMove(boolean left, boolean right, boolean up, boolean down, boolean jump, boolean attack) {
        //Vertical speed

        if (up || down || climbing) {
            speedx = 0;

            if (up && down || !up && !down) speedy = 0;

            else if (up && !down) {
                speedy = -3;
                climbing = true;
            }
    
            else if (down && !up) {
                speedy = 3;
                climbing = true;
            }

        }

        //Horizontal speed
        else if (!climbing && !hit) {
            if (left && right || !left && !right) speedx = 0;
        
            else if (left && !right) {
                speedx --;
                direction = -1;
            }

            else if (right && !left) {
                speedx ++;
                direction = 1;
            }
        }

        //Available moves when standing on platform
        if (standingOnPlatform) {
            if (jump && !climbing) { 
                jumping = true;
            }

            if (attack && !climbing) attacking = true;
            else if (speedx != 0) {
                costumeManager.run();
            }

            else {
                state = State.STANDING;
            }

            if (!intangible) climbing = false;
        }

        //Animation loops
        if (hit) {
            hit = costumeManager.damaged();
        }

        else if (climbing) {
            costumeManager.climb();
        }

        else if (jumping) {
            jumping = costumeManager.jump();
        }

        else if (attacking) {
            attacking = costumeManager.attack();
        }

        else if (dying && standingOnPlatform) {
            dead = costumeManager.death();
        }


        //Falling
        else if (!jump && !standingOnPlatform) {
            state = State.FALLING;
        }

        if (!climbing) speedy += gravity;
        
        checkMaxSpeed();
        costumeManager.updateCostume();
    }

    /**
     * Checks if the bottom of the hitbox of the {@code Player} is within the hitbox of a given {@link ScreenObject}.
     * @param screenObject the {@link ScreenObject} to check
     */
    public boolean midpointBotomIsColliding(ScreenObject screenObject) {

        boolean midpointBottomIsColliding = false;

        if (screenObject.getHitbox().contains(midpointBotom)) {
            midpointBottomIsColliding = true;
        }

        return midpointBottomIsColliding;

    }

    /**
     * Sets the {@code Player}'s value for standing on a platform to the given parameter.
     * @param standingOnPlatform true or false
     */
    public void setStandingOnPlatform(boolean standingOnPlatform) {
        this.standingOnPlatform = standingOnPlatform;
    }

    /** @return whether or not the {@code Player} has detected that it is standing on a platform. */
    public boolean isStandingOnPlatform() {
        return standingOnPlatform;
    }

    /**
     * Checks if the {@code Player} hit a platform when falling by checking if the midpoint 
     * of the bottom of the hitbox is on the {@link Platform}. If not, the Player is
     * moved away from the platform in the horizontal direction to make it look like the Player
     * slipped off the edge of the platform, and the fall will then continue.
     * @param platform the {@link Platform} to check
     * @param deltay the distance between the player y-coordinate and the collision location
     */
    private void checkFallingOnPlatform(ScreenObject platform, int deltay) {

        //Adjust the midpoint of the hitbox to check if it within the platform
        setMidpointBotom();
    
        //Stop the fall if the midpoint is on the platform, take fall-damage according to the speed
        if (platform.getHitbox().contains(midpointBotom)) {
            deltay--;
            hitbox.y--;

            if (speedy > 30) damaged(50, -speedx);
            else if (speedy > 25) damaged(25, -speedx);

            else speedy = 0;

            y += deltay - speedy;
        }

        //Have the player slip off the platform edge
        else if (!platform.getHitbox().contains(midpointBotom)) {

            //Figure out the distance from the midpointBottom to the edges of the platform
            int distancePlatformX = midpointBotom.x - platform.getHitbox().x;
            int distancePlatformWidth = midpointBotom.x - (platform.getHitbox().x + platform.getDimension().width);

            //If player is to the right of platform, shove away by the smallest distance (as the distance is positive)
            if (distancePlatformX > 0 || distancePlatformWidth > 0) {
                hitbox.x += (hitbox.width / 2) - Math.min(distancePlatformX, distancePlatformWidth);
            }

            //If player is to the left of platform, shove away by the largest distance (as the distance is negative)
            else if (distancePlatformX < 0 || distancePlatformWidth < 0) {
                hitbox.x -= (hitbox.width / 2) - Math.max(distancePlatformX, distancePlatformWidth);
            }
        }
    }

    /**
     * Makes sure that the {@code Player} doesn't climb above a {@link Ladder}.
     * @param ladder the {@link Ladder} that is being climbed in
     */
    private boolean ladderLimits(ScreenObject ladder) {

        if (this.isColliding(ladder)) {
            midpointBotom.y += speedy;

            if (midpointBotom.y <= ladder.getY()) {
                
                int deltay = 0;
                midpointBotom.y -= speedy;

                while (midpointBotom.y > ladder.getY()) {
                    midpointBotom.y --;
                    deltay --;
                }

                speedy = 0;
                y += deltay;
                climbing = false;
            }
        }

        return this.isColliding(ladder);

    }

    @Override
    public boolean checkVerticalCollision(ScreenObject collider) {

        //Special check for when going up ladders
        if (collider.getClass() == Ladder.class && speedy < 0) {
            return ladderLimits(collider);
        }

        //Do not check ladder collision when climbing down or passing by ladder
        else if (collider.getClass() == Ladder.class && speedy >= 0) {
            return false;
        }

        //Do not collision check platforms when player is intangible and climbing
        else if (collider.getClass() == Platform.class && intangible && climbing) {
            return false;
        }

        return super.checkVerticalCollision(collider);
    }

    @Override
    protected void reactToVerticalCollision(ScreenObject collider, int deltay) {

        //platform collision when falling
        if (collider.getClass() == Platform.class && speedy > 0) {
            checkFallingOnPlatform(collider, deltay);
        }

        else {
            super.reactToVerticalCollision(collider, deltay);
        }
    }

    @Override
    public boolean checkHorizontalCollision(ScreenObject collider) {
        if (collider.getClass() != Ladder.class) {
            return super.checkHorizontalCollision(collider);
        }

        return false;
    }

    @Override
    protected void reactToHorizontalCollision(ScreenObject collider, int deltax) {
        if(collider.getClass() != Ladder.class) super.reactToHorizontalCollision(collider, deltax);
    }

    /** @return the {@link Projectile} that is the weapon of the {@code Player} */
    public Projectible getWeapon() {
        return weapon;
    }

    /**
     * Sets the weapon of the player to a specified {@Projectile}. 
     * @param weapon the projectile to set as weapon
     * @see {@link ProjectileType}
     */
    public void setWeapon(Projectible weapon) {
        this.weapon = weapon;
    }


    private void drawLives(Graphics g) {
        for (LifeHeart life : lives) {
            life.draw(g);
        }
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);

        drawLives(g);

        // Font font = new Font("SansSerif", Font.BOLD, 10);
        // g.setFont(font);
        // g.setColor(Color.BLACK);
        //GraphicHelperMethods.drawCenteredString(g, "Health: " + currentHealth, 450 + HOCModel.getOffset(), 10, 50, 30);
    }
}

