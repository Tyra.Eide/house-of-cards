package inf101v22.house_of_cards.screenObjects.sprites.player;

/**
 * A costume manager for a {@link Player}.
 * Keeps track of all movement animations and sets the current costume
 * based on the {@link Player}'s direction and state.
 * @author Tyra Fosheim Eide
 */
public class PlayerCostumeManager {

    private Player player;
    
    private int runCounter = 0;
    private int runNum = 0;

    private int jumpCounter = 0;

    private int attackCounter = 0;

    private int climbCounter = 0;
    private int climbNum = 0;

    private int damageCounter = 0;
    private int damageNum = 0;

    private int deathCounter = 0;
    private int deathNum = 0;

    /**
     * Creates a new {@code costume manager} for a given {@link Player}.
     * @param player
     */
    public PlayerCostumeManager(Player player) {
        this.player = player;
    }
    
    /**
     * Sets the correct current costume for the {@link Player} given its state and direction.
     */
    protected void updateCostume() {
        //Standing costumes
        if (player.getState() == State.STANDING) {
            if (player.getDirection() == -1) player.setCurrentCostume(player.getCostumeList().get(3));
            else if (player.getDirection() == 1) player.setCurrentCostume(player.getCostumeList().get(0));
        }        
        
        //Running costumes
        else if (player.getState() == State.RUNNING) {
            if (player.getDirection() == -1) {
                if (runNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(4));
                }
                else if (runNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(5));
                }
            }
    
            else if (player.getDirection() == 1) {
                if (runNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(1));
                }
                else if (runNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(2));
                }
            }
        }

        //Charging costume
        else if (player.getState() == State.CHARGING) {
            if (player.getDirection() == -1) {
                player.setCurrentCostume(player.getCostumeList().get(9));
            }

            else if (player.getDirection() == 1) {
                player.setCurrentCostume(player.getCostumeList().get(6));
            }
        }

        //Jumping costumes
        else if (player.getState() == State.JUMPING) {
            if (player.getDirection() == -1) {
                player.setCurrentCostume(player.getCostumeList().get(10));
            }

            else if (player.getDirection() == 1) {
                player.setCurrentCostume(player.getCostumeList().get(7));
            }
        }

        //Falling costumes
        else if (player.getState() == State.FALLING) {
            if (player.getDirection() == -1) {
                player.setCurrentCostume(player.getCostumeList().get(11));
            }

            else if (player.getDirection() == 1) {
                player.setCurrentCostume(player.getCostumeList().get(8));
            }
        }

        //Attacking costumes
        else if (player.getState() == State.ATTACKING) {
            if (player.getDirection() == -1) {
                player.setCurrentCostume(player.getCostumeList().get(13));
            }

            else if (player.getDirection() == 1) {
                player.setCurrentCostume(player.getCostumeList().get(12));
            }
        }

        //Climbing costumes
        else if (player.getState() == State.CLIMBING) {
            if (climbNum == 0) {
                player.setCurrentCostume(player.getCostumeList().get(14));
            }
            else if (climbNum == 1) {
                player.setCurrentCostume(player.getCostumeList().get(15));
            }
        }

        //Damage costumes
        else if (player.getState() == State.DAMAGED) {
            if (player.getDirection() == -1) {
                if (damageNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(11));
                }
                else if (damageNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(16));
                }
            }
    
            else if (player.getDirection() == 1) {
                if (damageNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(8));
                }
                else if (damageNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(17));
                }
            }
        }

        //Death costumes
        else if (player.getState() == State.DEAD) {
            if (player.getDirection() == 1) {
                if (deathNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(18));
                }

                else if (deathNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(19));
                }

                else if (deathNum == 2) {
                    player.setCurrentCostume(player.getCostumeList().get(20));
                }

                else if (deathNum == 3) {
                    player.setCurrentCostume(player.getCostumeList().get(21));
                }

                else if (deathNum == 4) {
                    player.setCurrentCostume(player.getCostumeList().get(22));
                }

                else if (deathNum == 5) {
                    player.setCurrentCostume(player.getCostumeList().get(23));
                }
            }

            else if (player.getDirection() == -1) {
                if (deathNum == 0) {
                    player.setCurrentCostume(player.getCostumeList().get(24));
                }

                else if (deathNum == 1) {
                    player.setCurrentCostume(player.getCostumeList().get(25));
                }

                else if (deathNum == 2) {
                    player.setCurrentCostume(player.getCostumeList().get(26));
                }

                else if (deathNum == 3) {
                    player.setCurrentCostume(player.getCostumeList().get(27));
                }

                else if (deathNum == 4) {
                    player.setCurrentCostume(player.getCostumeList().get(28));
                }

                else if (deathNum == 5) {
                    player.setCurrentCostume(player.getCostumeList().get(29));
                }
            }
        }
    }

    /** Tracks where the {@code Player} is in the running animation.*/
    protected void run() {
        player.setState(State.RUNNING);

        //update the running counter that controls running animation
        runCounter++;
        if (runCounter > 4) {
            if (runNum == 0) {
                runNum = 1;
            }
    
            else if (runNum == 1) {
                runNum = 0;
            }
            runCounter = 0;
        }
    }

    /** 
     * {@link Player} jumping animation.
     * @return true if the jump is ongoing, false if the jump is finished.
     */
    protected boolean jump() {
    
        //charge jump
        if (jumpCounter < 3) {
            player.setState(State.CHARGING);;
            player.setSpeed(0, 0);
            jumpCounter++;
        }

        //take off
        else if (jumpCounter == 3) {
            player.adjustSpeed(player.getSpeedx(), -15);
            jumpCounter++;
        }

        //during the jump
        else if (jumpCounter == 4) {
            
            //check if player has landed
            if (player.isStandingOnPlatform()) {
                player.setSpeed(0, 0);    
                jumpCounter++;
            }

            //going up
            if (player.getSpeedy() <= 0) {
                player.setState(State.JUMPING);
            }

            //falling down
            else if (player.getSpeedy() > 0) {
                player.setState(State.FALLING);
            }
        }

        //landing
        else if (jumpCounter >= 5 && jumpCounter < 7) {
            player.setState(State.CHARGING);
            player.setSpeed(0, 0);
            jumpCounter++;
        }

        //finish the jump
        else if (jumpCounter == 7) {
            player.setState(State.STANDING);
            jumpCounter = 0;
            return false;
        }

        return true;
    }

    /** Tracks where the {@code Player} is in the attack animation. */
    protected boolean attack() {
        player.setSpeed(0, 0);

        if (attackCounter < 6) {
            player.setState(State.ATTACKING);
            attackCounter++;
        }

        else if (attackCounter >= 6) {
            attackCounter = 0;
            return false;
        }

        return true; 
    }

    /** Tracks where the {@link Player} is in the climbing animation. */
    protected void climb() {
        player.setState(State.CLIMBING);

        //play climbing animation as long as the player is moving
        if (player.getSpeedy() != 0) {
            //update the climbing counter that controls climbing animation
            climbCounter++;
            if (climbCounter > 4) {
                if (climbNum == 0) {
                    climbNum = 1;
                }
        
                else if (climbNum == 1) {
                    climbNum = 0;
                }

                climbCounter = 0;
            }
        }

    }

    /** 
     * Keeps track of the damage animation for the {@code Player}. 
     * @return true if the damage animation is ongoing, false if it has finished.
     */
    protected boolean damaged() {
        player.setState(State.DAMAGED);
        
        damageCounter++;
        if (damageCounter % 5 == 0) {
            if (damageNum == 0) {
                damageNum = 1;
            }
    
            else if (damageNum == 1) {
                damageNum = 0;
            }
        }

        else if (damageCounter > 20) {
            damageCounter = 0;
            return false;
        }

        return true;
    }

    /**
     * Keeps track of the death animatiom for the {@code Player}.
     * @return true if the death animation haas finished, false if not
     */
    protected boolean death() {
        player.setState(State.DEAD);
        player.setSpeed(0, 0);

        deathCounter++;
        if (deathCounter % 4 == 0 && deathCounter <= 20) {
            deathNum++;
        }

        else if (deathCounter > 20) {
            return true;
        }

        return false;
    }
}
