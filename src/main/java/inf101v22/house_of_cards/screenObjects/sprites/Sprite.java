package inf101v22.house_of_cards.screenObjects.sprites;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.view.GraphicHelperMethods;

/**
 * A general Sprite object. Has a list of costumes and a speed.
 * Extends {@link ScreenObject}.
 * @author Tyra Fosheim Eide
 */
public class Sprite extends ScreenObject{
    protected ArrayList<BufferedImage> costumeList;
    protected BufferedImage currentCostume;
    
    protected int speedx = 0;
    protected int speedy = 0;

    protected boolean visible = true;

    /**
     * Creates a new Sprite at a specified position with a specified spritesheet
     * @param x coordinate
     * @param y coordinate
     * @param spriteRow the number of costumes per row in the sprite sheet
     * @param spriteCol the number of costumes per column in the sprite sheet
     * @param spriteSheetpath the path of the sprite sheet
     */
    public Sprite(int x, int y, int spriteRow, int spriteCol, String spriteSheetpath) {
        super(x, y, new Dimension(0,0));
        costumeList = GraphicHelperMethods.makeSpriteList(this, spriteSheetpath, spriteRow, spriteCol);
        dimension = new Dimension(costumeList.get(0).getWidth(),costumeList.get(0).getHeight());

        setHitbox(new Rectangle(x, y, dimension.width, dimension.height));
        currentCostume = costumeList.get(0);

    }

    /** @return the list of costumes belonging to the {@code Sprite}. */
    public ArrayList<BufferedImage> getCostumeList() {
        return costumeList;
    }

    /**
     * Sets the current costume of the {@code Sprite} to a specified image.
     * @param currentCostume the costume to be set
     */
    protected void setCurrentCostume(BufferedImage currentCostume) {
        this.currentCostume = currentCostume;
    }

    /** @return the image of the current costume of the {@code Sprite}. */
    public Image getCurrentCostume() {
        return currentCostume;
    }
    
    /**
     * Sets the speed of the {@code Sprite} to the specified value.
     * @param speedx the value to set the horisontal speed to
     * @param speedy the value to set the vertical speed to
     */
    public void setSpeed(int speedx, int speedy) {
        this.speedx = speedx;
        this.speedy = speedy;
    }

    /**
     * Adjusts the speed of the {@code Sprite} by the specified value.
     * @param speedx the amount to adjust the horisontal speed
     * @param speedy the amonut to adjust the vertical speed
     */
    public void adjustSpeed(int speedx, int speedy) {
        this.speedx += speedx;
        this.speedy += speedy;
    } 

    /**  
     * @return the horisontal speed of the {@code Sprite}.
     * A positive number tells that the speed is going to the right,
     * while a negative number tells that the speed is going to the left.
     */
    public int getSpeedx() {
        return speedx;
    }

    /**  
     * @return the vertical speed of the {@code Sprite}.
     * A positive number tells that the speed is going down,
     * while a negative number tells that the speed is going up.
     */
    public int getSpeedy() {
        return speedy;
    }

    /**
     * Changes the visibility of the {@code Sprite}.
     * @param visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /** @return true if the {@code Sprite} is visible, false if not. */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Checks if the {@code Sprite} is about to collide vertically with a given {@link ScreenObject}.
     * @param collider the {@link ScreenObject} to check for collision
     * @return true if a collision was detected, false if not.
     */
    public boolean checkVerticalCollision(ScreenObject collider) {

        //check for collision in the next move
        hitbox.y += speedy;

        if (this.isColliding(collider)) {
            //Move hitbox back to starting point
            //Find the closest possible location that does not cause a collision
            hitbox.y -= speedy;

            int deltay = 0; //counts the distance to move the actor
            int directiony = (int)Math.signum(speedy); //finds the direction the actor is moving

            //Move one step at a time until the collision occurs
            while (!this.isColliding(collider)) {
                hitbox.y += directiony;
                deltay += directiony;
            }

            reactToVerticalCollision(collider, deltay);

            return true;
        }

        hitbox.y -= speedy;

        return false;
    }


    /** 
     * The {@code Sprite}'s reaction to vertical collision.
     * @param collider the {@link ScreenObject that is colliding with the {@code Sprite}
     * @param deltay the distance from the {@code Sprite}'s y-coordinate to the {@link ScreenObject}'s y-coordinate
     */
    protected void reactToVerticalCollision(ScreenObject collider, int deltay) {

            int directiony = (int)Math.signum(speedy);

            //Move back a step so the actor's hitbox stands on top of the collider
            hitbox.y -= directiony;
            deltay -= directiony;

            //Move the actor to the location and set the speed to zero
            y += deltay;
            speedy = 0;
    }


    /**
     * Checks if the {@code Sprite} is about to collide vertically with a given {@link ScreenObject}.
     * @param collider the {@link ScreenObject} to check for collision
     * @return true if a collision was detected, false if not.
     */
    public boolean checkHorizontalCollision(ScreenObject collider) {
        
        //check for collision in the next move
        hitbox.x += speedx;

        if (this.isColliding(collider)) {
            //Move hitbox back to starting point
            //Find the closest possible location that does not cause a collision
            hitbox.x -= speedx;

            int deltax = 0; //counts the distance to move the actor
            int directionx = (int)Math.signum(speedx); //finds the direction the actor is moving

            //Move one step at a time until the collision occurs
            while (!this.isColliding(collider)) {
                hitbox.x += directionx;
                deltax += directionx;
            }

            reactToHorizontalCollision(collider, deltax);

            return true;
        }

        hitbox.x -= speedx;

        return false;
    }

    /** 
     * The {@code Sprite}'s reaction to horizontal collision.
     * @param collider the {@link ScreenObject that is colliding with the {@code Sprite}
     * @param deltax the distance from the {@code Sprite}'s x-coordinate to the {@link ScreenObject}'s x-coordinate
     */
    protected void reactToHorizontalCollision(ScreenObject collider, int deltax) {
        
        int directionx = (int)Math.signum(speedx);

        //Move back a step so the actor's hitbox stands on top of the collider
        hitbox.x -= directionx;
        deltax -= directionx;

        //Move the actor to the location and set the speed to zero
        x += deltax;
        speedx = 0;
    }

    /**
     * Draws the {@code Sprite} on a panel
     * @param g the panel to draw on
     */
    public void draw(Graphics g) {
        g.drawImage((Image)currentCostume, x, y, null);
    }


}
