package inf101v22.house_of_cards.screenObjects.sprites.monsters.guards;

import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;

/**
 * A HeartGuard in House of Cards.
 * Extends {@link Monster}.
 * @author Tyra Fosheim Eide
 */
public class HeartGuard extends Monster {
    public static final String SPRITESHEET_PATH = "sprites/Guards/Heart Guard Spritesheet.png";
    public static final int SPRITE_ROW = 4;
    public static final int SPRITE_COLUMN = 2;

    public static final int damage = 100;
    public static final int speed = 4;

    public static final int health = 300;

    /**
     * Creates a new HeartGuard at the given position with the given direction and range
     * @param x coordinate
     * @param y coordinate
     * @param direction 1 for right, 0 for standing still, -1 for left
     * @param range the range the guard can move before turning around
     */
    public HeartGuard(int x, int y, int direction, int range) {
        super(x, y, SPRITE_ROW, SPRITE_COLUMN, SPRITESHEET_PATH, range);
        
        super.direction = direction;
        
        super.range = range;
        super.damage = damage;
        super.speed = speed;
        super.health = health;

        setHitbox();

        setSpeed(speed * direction, 0);
    }
}
