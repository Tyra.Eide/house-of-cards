package inf101v22.house_of_cards.screenObjects.sprites.monsters;

import java.awt.Rectangle;

import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.screenObjects.sprites.Damageable;
import inf101v22.house_of_cards.screenObjects.sprites.Sprite;

// A reflection note about the Monster class:
// I originally intended to include bosses and armed guards in the game,
// but decided to start with only unarmed guards, as I realized I would be stumped for time.
// I thought I could just expand this class to include all types of monsters
// eventually, but I think I would have had to find another soloution if I had more time to try.
// I probably would go with something like collecting all general monster methods like planMove()
// and the damagable methods and setHitbox and so on in an interface, IMonster, and then I would have
// made a class for each type of monster. I would have made the types of monsters like I did with 
// Projectibles, probably, so the spesific kinds of monsters are just information-storers.
// I did not get time to do this, and personally believe that the code I have now is not that
// well organized. I hope my explanation here can be taken into consideration when reviewing my code :)


/**
 * A basic monster of House of Cards. Can walk around or stand still,
 * and deals damage to {@link Damageable} objects on impact. Can also take
 * damage and die.
 * Extends {@link Sprite} and implements {@link Damageable}
 * @author Tyra Fosheim Eide
 */
public class Monster extends Sprite implements Damageable {

    protected int direction;

    protected int range;
    protected int damage;
    protected int speed;

    protected int health;
    protected boolean hit;
    protected boolean dead;

    private int rangeCounter = 0;

    private int costumeCounter = 0;
    private int costumeNumber = 0;

    private int damageCounter = 0;

    /**
     * Creates a new monster at the given position and with a given spriteSheet and range
     * @param x coordinate
     * @param y coordinate
     * @param spriteRow the number of costumes per row in the sprite sheet
     * @param spriteCol the number of costumes per column in the sprite sheet
     * @param spriteSheetpath the path of the sprite sheet
     * @param range the range the monster can walk before turning
     */
    public Monster(int x, int y, int spriteRow, int spriteCol, String spriteSheetpath, int range) {
        super(x, y, spriteRow, spriteCol, spriteSheetpath);

        this.range = range;
        setHitbox();

        setVisible(true);
    }

    /** Sets the hitbox of the Monster. */
    public void setHitbox() {
        if (direction == 0 && !hit && costumeNumber == 1) {
            hitbox = new Rectangle(x + 16, y + 7, dimension.width - 32, dimension.height - 6);
        }

        else {
            hitbox = new Rectangle(x + 16, y + 3, dimension.width - 32, dimension.height - 3);
        }
    }

    /** Updates the current costume of the {@code Monster} based on animation counters and direction. */
    private void updateCostume() {
        if (hit) {
            if (costumeNumber == 0) {
                setCurrentCostume(costumeList.get(6));
            }

            else if (costumeNumber == 1) {
                setCurrentCostume(costumeList.get(7));
            }
        }

        else if (direction == -1) {
            if (costumeNumber == 0) {
                setCurrentCostume(costumeList.get(4));
            }

            if (costumeNumber == 1) {
                setCurrentCostume(costumeList.get(5));
            }
        }

        else if (direction == 0) {
            if (costumeNumber == 0) {
                setCurrentCostume(costumeList.get(0));
            }

            if (costumeNumber == 1) {
                setCurrentCostume(costumeList.get(1));
            }
        }
        
        else if (direction == 1) {
            if (costumeNumber == 0) {
                setCurrentCostume(costumeList.get(2));
            }

            if (costumeNumber == 1) {
                setCurrentCostume(costumeList.get(3));
            }
        }

    }

    /** Swaps the costume number from 1 to 0 or vice versa. */
    private void swapCostumeNumber() {
        if (costumeNumber == 0) {
            costumeNumber = 1;
        }

        else if (costumeNumber == 1) {
            costumeNumber = 0;
        }
    }

    /** Keeps track of the walking and standing animation of the {@code Monster}. */
    private void costumeAnimation() {
        costumeCounter++;
        if (direction == 0) {
            if (costumeCounter > 20) {
                swapCostumeNumber();

                costumeCounter = 0;
            }
        }

        else if (direction != 0) {
            if (costumeCounter > 10) {
                swapCostumeNumber();

                costumeCounter = 0;
            }
        }
    }

    /** @return true if the monster is dead, false if not. */
    public boolean isDead() {
        return dead;
    }

    /** Checks if the monster's health is depleated, and sets the monster to dead if it is. */
    private void checkHealth() {
        if (health <= 0) {
            dead = true;
        }

        else {
            hit = false;
        }
    }

    /** Keeps track of the damage animation for the {@code Monster}. */
    private void damageAnimation() {
        damageCounter++;
        if (damageCounter % 5 == 0) {
            swapCostumeNumber();
        }

        else if (damageCounter > 20) {
            damageCounter = 0;
            checkHealth();
        }
    }

    /** 
     * Walking loop of the {@code Monster}. 
     * Makes sure that the {@code Monster} doesn't exceed its range and that
     * it turns upon reaching the range. 
     */
    private void walk() {
        if (rangeCounter + speed <= range) {
            setSpeed(speed * direction, 0);
            rangeCounter += speed;
        }

        else if (rangeCounter == range) {
            costumeCounter = 0;
            direction *= -1;
            rangeCounter = 0;
        }

        else if (rangeCounter + speed > range) {
            rangeCounter += range - rangeCounter;
            setSpeed((range - rangeCounter) * direction, 0);
        }
    }

    /** Prepares a new move of the {@code Monster} by adjusting the speed according to direction and range. */
    public void planMove() {

        if (hit) {
            setSpeed(0, 0);
            damageAnimation();
        }

        else if (!hit) {
            if (direction != 0) {
                walk();
            }

            costumeAnimation();
        }

        updateCostume();
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public void damaged(int damage, int colliderDirection) {
        if (!hit) {
            health -= damage;
            hit = true;
        }
    }

    @Override
    public ScreenObject getScreenObject() {
        return this;
    }

    /**
     * Checks if the {@code Monster} hit a {@link Damagable} object.
     * Tells the {@link Damagable} object that it has been hit by the {@code Monster}.
     * @param damagable the object to check for hit
     * @return true if the projectile hit, false if not
     */
    public boolean checkHit(Damageable damagable) {
        boolean hit = false;
            
        //check for collision
        if ((this.checkHorizontalCollision(damagable.getScreenObject()) || this.checkVerticalCollision(damagable.getScreenObject()))) {
            damagable.damaged(this.damage, direction);
            hit = true;
        }

        return hit;
    }

    @Override
    public boolean isHit() {
        return hit;
    }




}
