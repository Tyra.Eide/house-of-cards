package inf101v22.house_of_cards.screenObjects.sprites.monsters.guards;

import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;

/**
 * A DiamondGuard in House of Cards.
 * Extends {@link Monster}.
 * @author Tyra Fosheim Eide
 */
public class DiamondGuard extends Monster{
    public static final String SPRITESHEET_PATH = "sprites/Guards/Diamond Guard Spritesheet.png";
    public static final int SPRITE_ROW = 4;
    public static final int SPRITE_COLUMN = 2;

    public static final int damage = 50;
    public static final int speed = 3;

    public static final int health = 200;

    /**
     * Creates a new DiamondGuard at the given position with the given direction and range
     * @param x coordinate
     * @param y coordinate
     * @param direction 1 for right, 0 for standing still, -1 for left
     * @param range the range the guard can move before turning around
     */
    public DiamondGuard(int x, int y, int direction, int range) {
        super(x, y, SPRITE_ROW, SPRITE_COLUMN, SPRITESHEET_PATH, range);
        
        super.direction = direction;
        
        super.range = range;
        super.damage = damage;
        super.speed = speed;
        super.health = health;

        setHitbox();

        setSpeed(speed * direction, 0);
    }
}
