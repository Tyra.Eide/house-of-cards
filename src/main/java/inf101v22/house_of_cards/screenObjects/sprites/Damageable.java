package inf101v22.house_of_cards.screenObjects.sprites;

import inf101v22.house_of_cards.screenObjects.ScreenObject;

/** 
 * All methods neccesary to inform an object that it has been damaged.
 * @author Tyra Fosheim Eide 
 */
public interface Damageable {
    
    /**
     * Informs the {@code Damagable} that it has been hit by something
     * and that it has taken a given amount of damage.
     * @param damage the amount to subtract from the {@code Damagable}'s health
     * @param colliderDirection the direction of the object that hit the {@code Damagable} 
     */
    public void damaged(int damage, int colliderDirection);

    /** @return the ScreenObject of a {@code Damagable} object for collision checking. */
    public ScreenObject getScreenObject();

    /** @return whether or not the  {@code Damagable} object is hit or not */
    public boolean isHit();

    /** @return the health of the {@code Damagable}. */
    public int getHealth();
}
