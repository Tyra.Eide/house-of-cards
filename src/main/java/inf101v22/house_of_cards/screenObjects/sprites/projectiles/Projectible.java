package inf101v22.house_of_cards.screenObjects.sprites.projectiles;

/**
 * Methods needed to get information about Projectile types.
 * This includes information about sprite sheet and stats for
 * range, damage and speed.
 * @author Tyra Fosheim Eide
 */
public interface Projectible {
    
    public String getSpritesheetPath();

    public int getSpriteRow();

    public int getSpriteColumn();

    public boolean canHurtPlayer();

    public int getRange();

    public int getDamage();

    public int getSpeed();
}
