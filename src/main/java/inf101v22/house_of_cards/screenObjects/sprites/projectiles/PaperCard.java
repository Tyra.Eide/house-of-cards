package inf101v22.house_of_cards.screenObjects.sprites.projectiles;


/**
 * Stores information about the projectile type PaperCard.
 * Implements {@link Projectible}
 * @author Tyra Fosheim Eide
 */
public class PaperCard implements Projectible {
    private final String SPRITESHEET_PATH = "sprites/Projectiles/Paper cards Spritesheet.png";
    private final int SPRITE_ROW = 2;
    private final int SPRITE_COLUMN = 4;

    private final boolean CAN_HURT_PLAYER = false;
    
    private final int range = 52;
    private final int damage = 100;
    private final int speed = 3;

    @Override
    public String getSpritesheetPath() {
        return SPRITESHEET_PATH;
    }

    @Override
    public int getSpriteRow() {
        return SPRITE_ROW;
    }

    @Override
    public int getSpriteColumn() {
        return SPRITE_COLUMN;
    }

    @Override
    public boolean canHurtPlayer() {
        return CAN_HURT_PLAYER;
    }

    @Override
    public int getRange() {
        return range;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public int getSpeed() {
        return speed;
    }
}
