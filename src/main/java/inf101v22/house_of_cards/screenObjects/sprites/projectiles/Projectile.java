package inf101v22.house_of_cards.screenObjects.sprites.projectiles;

import java.awt.Rectangle;

import inf101v22.house_of_cards.screenObjects.ScreenObject;
import inf101v22.house_of_cards.screenObjects.sprites.Damageable;
import inf101v22.house_of_cards.screenObjects.sprites.Sprite;

/**
 * A Projectile of House of Cards. Can be shot a certain range before being exhausted,
 * and deals damage upon impact with a {@link Damageable}.
 * Extends {@link Sprite}.
 * @author Tyra Fosheim Eide
 */
public class Projectile extends Sprite {

    private int direction;

    //private boolean canHurtPlayer; //opens possibility to have monsters shooting at the player in later extensions

    private int range;
    private int damage;
    private int speed;

    private int rangeCounter;
    
    private int costumeCounter;
    private int costumeNumber;

    private boolean exhausted;
    private boolean hitting;

    /**
     * Creates a new Projectile at a given position of a given type and direction
     * @param x coordinate
     * @param y coordinate
     * @param weapon the {@link Projectible} type of weapon
     * @param direction 1 for right, -1 for left
     */
    public Projectile(int x, int y, Projectible weapon, int direction) {
        super(x, y, weapon.getSpriteRow(), weapon.getSpriteColumn(), weapon.getSpritesheetPath());
        this.direction = direction;

        //this.canHurtPlayer = weapon.canHurtPlayer();
        this.range = weapon.getRange();
        this.damage = weapon.getDamage();
        this.speed = weapon.getSpeed();

        initialize();

        setVisible(true);

    }

    private void initialize() {
        rangeCounter = 0;
        costumeCounter = 0;
        costumeNumber = 0;

        exhausted = false;
        hitting = false;

        setSpeed(direction*speed, 0);
    }

    public int getRange() {
        return range;
    }

    public int getDamage() {
        return damage;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isExhausted() {
        return exhausted;
    }

    public boolean isHitting() {
        return hitting;
    }

    /** Updates the costume of the {@code PaperCard} based on its state and direction. */
    private void updateCostume() {

        //Hitting costumes
        if (hitting) {
            if (direction == 1) {
                setCurrentCostume(costumeList.get(5));
            }

            else if (direction == 0) {
                setCurrentCostume(costumeList.get(7));
            }

            else if (direction == -1) {
                setCurrentCostume(costumeList.get(6));
            }
        }

        //Shooting costumes
        else if (!exhausted) {
            if (direction == 1) {
                if (costumeNumber == 0) {
                    setCurrentCostume(costumeList.get(0));
                }

                else if (costumeNumber == 1) {
                    setCurrentCostume(costumeList.get(1));
                }
            }

            else if (direction == -1) {
                if (costumeNumber == 0) {
                    setCurrentCostume(costumeList.get(2));
                }

                else if (costumeNumber == 1){
                    setCurrentCostume(costumeList.get(3));
                }
            }
        }

        //Exhausted costumes
        else if (exhausted) {
            setCurrentCostume(costumeList.get(4));
        }
    }

    private void shootingAnimation() {
        costumeCounter++;
        if (costumeCounter > 2) {
            if (costumeNumber == 0) {
                costumeNumber = 1;
            }
            
            else if (costumeNumber == 1) {
                costumeNumber = 0;
            }
            
            costumeCounter = 0;
        }
    }

    /** What to do when told that the {@code Projectile} has hit. */
    private void hit() {
        if (costumeCounter < 15) {
            costumeCounter++;
        }

        else if (costumeCounter >= 15) {
            setVisible(false);
        }
    }

    /** The {@code Projectile} instructions for shooting. */
    private void shoot() {
        if (rangeCounter + speed <= range) {
            rangeCounter += speed;
        }

        else if (rangeCounter == range) {
            costumeCounter = 0;
            exhausted = true;
        }

        else if (rangeCounter + speed > range) {
            rangeCounter += range - rangeCounter;
            setSpeed((range - rangeCounter) * direction, 0);
        }

        shootingAnimation();
    }

    /** What to do when exhausted. */
    private void exhausted() {
        setSpeed(0, 1);
        direction = 0;

        if (costumeCounter > 10) {
            hitting = true;
        }

        costumeCounter++;
    }

    /** Plans the next move of the {@code Projectile}. */
    public void planMove() {

        if (hitting) {
            hit();
        }

        else if (!exhausted) {
            shoot();
        }

        else if (exhausted) {
            exhausted();
        }

        updateCostume();
    }

    @Override
    public boolean checkHorizontalCollision(ScreenObject collider) {

        //move the projectile back and have it immediately hit if it spawns inside another object.
        if (this.isColliding(collider)) {

            int deltax = 0;

            while (this.isColliding(collider)) {
                hitbox.x -= Math.signum(speedx);
                deltax -= Math.signum(speedx);
            }

            reactToHorizontalCollision(collider, deltax);
            return true;
        }
        
        //only check collision if the projectile is not exhausted
        if (!exhausted) return super.checkHorizontalCollision(collider);

        return false;
        
    }

    @Override
    public void reactToVerticalCollision(ScreenObject collider, int deltay) {

        //Check if the vertical collision was due to the projectile falling onto the ScreenObject
        Rectangle bottomLine = new Rectangle(hitbox.x + 1, hitbox.y + hitbox.height, hitbox.x + hitbox.width - 2, 1);

        if(collider.getHitbox().intersects(bottomLine) && speedx == 0) {
            //in that case, set direction 0 to get correct hit costume
            direction = 0;
        }
        
        super.reactToVerticalCollision(collider, deltay);

        hitting = true;
        costumeCounter = 0;

    }

    @Override
    public void reactToHorizontalCollision(ScreenObject collider, int deltax) {
        super.reactToHorizontalCollision(collider, deltax);

        hitting = true;
        costumeCounter = 0;
    }

    /**
     * Checks if the {@code Projectile} hit a {@link Damagable} object.
     * Tells the {@link Damagable} object that it has been hit by the {@code Projectile}.
     * @param damagable the object to check for hit
     * @return true if the projectile hit, false if not
     */
    public boolean checkHit(Damageable damagable) {
        boolean hit = false;
        
            if (!hitting && !exhausted) {
            
                //check for collision
                if (this.checkHorizontalCollision(damagable.getScreenObject()) || this.checkVerticalCollision(damagable.getScreenObject())) {
                    damagable.damaged(this.damage, direction);
                    hit = true;
                }
            }

        return hit;
    }
}
