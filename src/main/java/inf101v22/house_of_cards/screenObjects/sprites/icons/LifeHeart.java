package inf101v22.house_of_cards.screenObjects.sprites.icons;

import inf101v22.house_of_cards.screenObjects.sprites.Sprite;

/**
 * A life heart of Hosue of Cards. Used to indicate the Health of something.
 * Extends {@link Sprite}.
 * @author Tyra Fosheim Eide
 */
public class LifeHeart extends Sprite {
    private static final String SPRITESHEET_PATH = "sprites/Icons/Life Heart.png";
    private static final int SPRITE_ROW = 3;
    private static final int SPRITE_COLUMN = 2;

    private int costumeCounter = 0;
    private int costumeNum = 0;
    private boolean dead = false;

    private int health = 100;
    private int leftOverDamage = 0;

    /**
     * Creates a new life heart at the given position
     * @param x coordinate
     * @param y coorinate
     */
    public LifeHeart(int x, int y) {
        super(x, y, SPRITE_ROW, SPRITE_COLUMN, SPRITESHEET_PATH);
        
    }

    /** Updates the costume of the heart according to the remaining health. */
    private void updateCostume() {
        if (health <= 25) {
            setCurrentCostume(costumeList.get(3));
        }

        else if (health <= 50) {
            setCurrentCostume(costumeList.get(2));
        }

        else if (health <= 75) {
            setCurrentCostume(costumeList.get(1));
        }

    }

    /**
     * Decreases the health of the heart and sets the costume accordingly.
     * Stores the amount of health that went below 0.
     * @param health the amount to decrease
     */
    public void decreaseHealth(int health) {
        this.health -= health;

        if (this.health <= 0) {
            dead = true;
            leftOverDamage = this.health * -1;
        }
    }

    /** @return the damage left over when the heart took more damage than it could. */
    public int getLeftOverDamage() {
        return leftOverDamage;
    }

    /** Animation that plays when all the health in the heart depleats. */
    private void deathAnimation() {
        costumeCounter++;

        if (costumeCounter % 5 == 0) {
            if (costumeNum == 0) {
                costumeNum = 1;
                setCurrentCostume(costumeList.get(4));
            }
    
            else if (costumeNum == 1) {
                costumeNum = 0;
                setCurrentCostume(costumeList.get(5));
            }
        }

        else if (costumeCounter > 20) {
            costumeCounter = 0;
            visible = false;
        }
    }

    /** @return true if the heart is in the state dead, false if not */
    public boolean isDead() {
        return dead;
    }

    /**
     * Sets the state dead of the heart
     * @param dead true or false.
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }

    /** Updates the lfe heart by updating the costume. */
    public void updateLifeHeart() {
        if (dead) {
            deathAnimation();
        }

        else {
            updateCostume();
        }
    }
}
