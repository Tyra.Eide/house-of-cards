package inf101v22.house_of_cards.screenObjects;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Graphics;

/** 
 * An object with a position and dimension.
 * @author Tyra Fosheim Eide
*/
public class ScreenObject {
    protected int x;
    protected int y;
    protected Dimension dimension;
    protected Rectangle hitbox;

    public ScreenObject(int x, int y, Dimension dimension) {
        this.x = x;
        this.y = y;
        this.dimension = dimension;
        this.hitbox = new Rectangle(x, y, dimension.width, dimension.height);
    }

    /**
     * Sets the position of a {@code screen object}.
     * @param x coordinate
     * @param y coordinate
     */
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Moves the position of the {@code screen object} by the given parameters,
     * @param x how much to move the x coordinate
     * @param y how much to move the y coordinate
     */
    public void movePosition(int x, int y) {
        this.x += x;
        this.y += y;

    }

    /** @return the x-coordinate of the {@code screen object}. */
    public int getX() {
        return x;
    }

    /** @return the y-coordinate of the {@code screen object}. */
    public int getY() {
        return y;
    }

    /** 
     * @return the {@link Dimension} of the {@code ScreenObject}.
     * @see {@link Dimension}
     */
    public Dimension getDimension() {
        return dimension;
    }

    /**
     * Sets the hitbox of the {@code ScreenObject}.
     * @param hitbox the new hitbox of the {@code ScreenObject}
     */
    public void setHitbox(Rectangle hitbox) {
        this.hitbox = hitbox;
    }

    /**
     * Moves the bounds of the {@code screen object} by the given parameters.
     * @param x amount to move x coordinate
     * @param y amount to move y coordinate
     */
    public void moveHitbox(int x, int y) {
        this.hitbox.x += x;
        this.hitbox.y += y;
    }

    /** @return the hitbox of the {@code screen object}. */
    public Rectangle getHitbox() {
        return hitbox;
    }

    /**
     * Checks if the {@code screen object} is intersecting
     * with another screen object by checking if their bounds
     * are intersecting.
     * @param collider the object to check for intersection
     * @return true if there is a collision, and false if not.
     */
    public boolean isColliding(ScreenObject collider) {
        return hitbox.intersects(collider.getHitbox());
    }

    /**
     * Draws the hitbox of the {@code Sprite} on a panel.
     * @param g the panel to draw on
     */
    protected void drawHitbox(Graphics g) {
        g.setColor(Color.RED);
        g.drawRect(hitbox.x, hitbox.y, hitbox.width, hitbox.height);
    }
}