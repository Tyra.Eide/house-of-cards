package inf101v22.house_of_cards;

import javax.swing.JFrame;

import inf101v22.house_of_cards.controller.HOCController;
import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.view.HOCView;

/** Main program of House of Cards. */
public class HOCMain {

    private static final String WINDOW_NAME = "House of Cards";
    private static JFrame frame;

    public static void main(String[] args) {

        HOCModel model = new HOCModel();
        HOCView view = new HOCView(model);

        new HOCController(model, view);
        
        frame = new JFrame(WINDOW_NAME);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

        frame.setContentPane(view);;

        frame.setVisible(true);
 
    }

    public static void closeWindow() {
        System.exit(0);
    }
    
}
