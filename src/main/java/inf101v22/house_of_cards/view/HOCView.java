package inf101v22.house_of_cards.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JComponent;

import inf101v22.house_of_cards.levelControl.LevelCategory;
import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.model.HOCModel;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectile;

/**
 * The view of a game of House of Cards.
 * Draws all the {@link ScreenObject}s of the game as well as
 * drawing level screens, intro screen and menus.
 * Extends {@link JComponent}
 * @author Tyra Fosheim Eide
 */
public class HOCView extends JComponent {
    private HOCViewable model;

    private final String ICONSHEET_PATH = "sprites/Icons/LevelCategory Iconsheet.png";

    private ArrayList<BufferedImage> levelCategoryIcons;

    {
        this.setFocusable(true);
        
    }

    public HOCView(HOCViewable model) {
        this.model = model;

        levelCategoryIcons = GraphicHelperMethods.makeSpriteList(this, ICONSHEET_PATH, 2, 2);
    }

    /** Draws the screen around the game panel */
    private void drawBackground(Graphics g) {
        //shade out the surrounding area of the game screen
        g.setColor(Color.BLACK);
        
        //left shade
        g.fillRect(0, 0, (Toolkit.getDefaultToolkit().getScreenSize().width - HOCModel.getPreferredSize().width) / 2, Toolkit.getDefaultToolkit().getScreenSize().height);

        //right shade
        g.fillRect(HOCModel.getOffset() + HOCModel.getPreferredSize().width, 0, (Toolkit.getDefaultToolkit().getScreenSize().width - HOCModel.getPreferredSize().width) / 2, Toolkit.getDefaultToolkit().getScreenSize().height);

    }

    /** draws the background of the game panel */
    private void drawGameScreenBackground(Graphics g, Color color) {
        g.setColor(color);
        g.fillRect(HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
    }

    private void drawNextLevelScreen(Graphics g) {
        drawGameScreenBackground(g, Color.BLACK);
        
        Font font = new Font("SansSerif", Font.BOLD, 25);
        g.setFont(font);

        g.setColor(Color.WHITE);

        //Ace level
        if (model.getLevelNumber() == 1) {
            GraphicHelperMethods.drawCenteredString(g, "A", HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
        }

        //Jack level
        else if (model.getLevelNumber() == 11) {
            GraphicHelperMethods.drawCenteredString(g, "J", HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
        }

        //Queen level
        else if (model.getLevelNumber() == 12) {
            GraphicHelperMethods.drawCenteredString(g, "Q", HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
        }

        //King level
        else if (model.getLevelNumber() == 13) {
            GraphicHelperMethods.drawCenteredString(g, "K", HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
        }

        //Numbered levels
        else {
            GraphicHelperMethods.drawCenteredString(g, "" + model.getLevelNumber(), HOCModel.getOffset(), 0, HOCModel.getPreferredSize().width, HOCModel.getPreferredSize().height);
        }

        g.drawImage(levelCategoryIcons.get(LevelCategory.getNumber(model.getLevelCategory())), HOCModel.getOffset() + HOCModel.getPreferredSize().width/2, HOCModel.getPreferredSize().height/2 - 32, null);
    }

    private void drawIntroductionScreen(Graphics g) {
        drawGameScreenBackground(g, Color.BLACK);

        g.setColor(Color.WHITE);
        Font font = new Font("SansSerif", Font.BOLD, 25);
        g.setFont(font);

        String introduction1 = "Long ago, the four kingdom of Decks lived in peaceful harmony.";
        String introduction2 = "The four Kings decided that they each wanted a tower of their own.";
        String introduction3 = "They appointed Janus the Joker to build the towers.";
        String introduction4 = "But when Janus finished, something terrible happened.";
        String introduction5 = "The four kingdoms had been cursed!";
        String introduction6 = "Help Janus to climb the towers and break the curse!";
        String introduction7 = "(Space to skip)";

        int textBoxHeight = HOCModel.getPreferredSize().height/2;

        GraphicHelperMethods.drawCenteredString(g, introduction1, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction2, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4 + textBoxHeight/6, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction3, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4 + textBoxHeight/6 * 2, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction4, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4 + textBoxHeight/6 * 3, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction5, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4 + textBoxHeight/6 * 4, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction6, HOCModel.getOffset(), HOCModel.getPreferredSize().height/4 + textBoxHeight/6 * 5, HOCModel.getPreferredSize().width, textBoxHeight/6);
        GraphicHelperMethods.drawCenteredString(g, introduction7, HOCModel.getOffset(), HOCModel.getPreferredSize().height - GraphicHelperMethods.getStringHeight(g, font, introduction7) * 2, HOCModel.getPreferredSize().width, GraphicHelperMethods.getStringHeight(g, font, introduction7));
    }

    private void drawInformationScreen(Graphics g) {
        g.setColor(Color.WHITE);
        Font font = new Font("SansSerif", Font.BOLD, 25);
        g.setFont(font);

        //control information
        GraphicHelperMethods.drawCenteredString(g, "Move: Arrows", HOCModel.getOffset() + HOCModel.getPreferredSize().width, 0, HOCModel.getOffset(), HOCModel.getPreferredSize().height - 150);
        GraphicHelperMethods.drawCenteredString(g, "Jump: Space", HOCModel.getOffset() + HOCModel.getPreferredSize().width, 0, HOCModel.getOffset(), HOCModel.getPreferredSize().height - 50);
        GraphicHelperMethods.drawCenteredString(g, "Attack: A", HOCModel.getOffset() + HOCModel.getPreferredSize().width, 0, HOCModel.getOffset(), HOCModel.getPreferredSize().height + 50);
        GraphicHelperMethods.drawCenteredString(g, "Pause: P", HOCModel.getOffset() + HOCModel.getPreferredSize().width, 0, HOCModel.getOffset(), HOCModel.getPreferredSize().height + 150);

        //level information
        if (model.getLevelNumber() == 1) {
            GraphicHelperMethods.drawCenteredString(
                g, "A", HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] * 8 / 9, 
                HOCModel.getItemBarBounds()[1], HOCModel.getItemBarBounds()[2]/10, HOCModel.getItemBarBounds()[3]
                );
        }

        else if (model.getLevelNumber() == 11) {
            GraphicHelperMethods.drawCenteredString(
                g, "J", HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] * 8 / 9, 
                HOCModel.getItemBarBounds()[1], HOCModel.getItemBarBounds()[2]/10, HOCModel.getItemBarBounds()[3]
                );
        }

        else if (model.getLevelNumber() == 12) {
            GraphicHelperMethods.drawCenteredString(
                g, "Q", HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] * 8 / 9, 
                HOCModel.getItemBarBounds()[1], HOCModel.getItemBarBounds()[2]/10, HOCModel.getItemBarBounds()[3]
                );
        }

        else if (model.getLevelNumber() == 13) {
            GraphicHelperMethods.drawCenteredString(
                g, "K", HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] * 8 / 9, 
                HOCModel.getItemBarBounds()[1], HOCModel.getItemBarBounds()[2]/10, HOCModel.getItemBarBounds()[3]
                );
        }

        else {
            GraphicHelperMethods.drawCenteredString(
                g, "" + model.getLevelNumber(), HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] * 8 / 9, 
                HOCModel.getItemBarBounds()[1], HOCModel.getItemBarBounds()[2]/10, HOCModel.getItemBarBounds()[3]
                );
        }

        g.drawImage(levelCategoryIcons.get(
            LevelCategory.getNumber(model.getLevelCategory())), 
            HOCModel.getItemBarBounds()[0] + HOCModel.getItemBarBounds()[2] - 49, -5, 51, 51, null
            );
    }

    private void drawFinishedScreen(Graphics g) {
        drawGameScreenBackground(g, Color.BLACK);
        model.drawMenu(g, Color.WHITE);
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);

        drawBackground(g);

        if (model.getState() == GameState.NEXT_LEVEL) {
            drawNextLevelScreen(g);
        }

        else if (model.getState() == GameState.START) {
            drawIntroductionScreen(g);
        }

        else if (model.getState() == GameState.FINISHED) {
            drawFinishedScreen(g);
        }

        else {
            drawGameScreenBackground(g, Color.GRAY);
        }
        


        if (model.getState() == GameState.WELCOME 
            || model.getState() == GameState.PAUSED 
            || model.getState() == GameState.GAME_OVER) {
            
                model.drawMenu(g, Color.BLACK);
        }
        
        if (model.getState() == GameState.ACTIVE) {
            
            
            if (model.getPlatforms() != null) {
                for (Platform platform: model.getPlatforms()) {
                    platform.draw(g);
                }
            }

            if (model.getLadders() != null) {
                for (Ladder ladder: model.getLadders()) {
                    ladder.draw(g);
                }
            }

            if (model.getMonsters() != null) {
                for (Monster monster : model.getMonsters()) {
                    monster.draw(g);
                } 
            }
            
            model.getDoor().draw(g);

            if (model.getPlayer().isVisible()) {
                model.getPlayer().draw(g);
            }

            if (model.getProjectiles() != null) {
                for (Projectile projectile : model.getProjectiles()) {
                    projectile.draw(g);
                }
            }

            //redraw background to make the player disappear as they walk out of the game screen.
            drawBackground(g);
            drawInformationScreen(g);
        }
    }


}
