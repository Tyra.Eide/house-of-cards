package inf101v22.house_of_cards.view;

import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Color;

import inf101v22.house_of_cards.levelControl.LevelCategory;
import inf101v22.house_of_cards.model.GameState;
import inf101v22.house_of_cards.screenObjects.levelDesign.Door;
import inf101v22.house_of_cards.screenObjects.levelDesign.Ladder;
import inf101v22.house_of_cards.screenObjects.levelDesign.Platform;
import inf101v22.house_of_cards.screenObjects.sprites.monsters.Monster;
import inf101v22.house_of_cards.screenObjects.sprites.player.Player;
import inf101v22.house_of_cards.screenObjects.sprites.projectiles.Projectile;

/**
 * All the methods needed to create a viewable game object.
 * This contains getters for game objects such as
 * player, monsters.
 * 
 * @author Tyra Fosheim Eide
 */
public interface HOCViewable {

    /** @return the {@link GameState} of the model. */
    public GameState getState();

    /** @return the {@link Player} of the model. */
    public Player getPlayer();

    /** @return a list of all the {@link Platform}s of the model. */
    public ArrayList<Platform> getPlatforms();

    /** @return a list of all the {@link Ladder}s of the model. */
    public ArrayList<Ladder> getLadders();

    /** @return a list of all the {@link Projectible}s of the model. */
    public ArrayList<Projectile> getProjectiles();

    /** @return a list of all the {@link Mosnter}s of the model. */
    public ArrayList<Monster> getMonsters();

    /** @return the current door of the model. */
    public Door getDoor();

    /** @return the current level number of the model. */
    public int getLevelNumber();

    /** @return the current levelCategory of the model. */
    public LevelCategory getLevelCategory();

    /** @return draws the current menu of the model. */
    public void drawMenu(Graphics g, Color titleColor);

}
